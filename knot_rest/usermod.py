from flask import Blueprint
from flask.cli import AppGroup

import click

from knot_rest.auth import add_admin, add_zoneacl, del_admin, del_zoneacl, set_description
from knot_rest.exceptions import KnotRestUserError

bp = Blueprint('usermod', __name__)
bp.cli.short_help = "User rights manipulation."
bp.cli.help = "This group of commands allows modify access rights for user."

zone_cli = AppGroup('zone')
zone_cli.short_help = "Modify user right to the zone."
zone_cli.help = "This group of commands allows modify user right to the zone."

@zone_cli.command("add", help="Add user rights to the zone")
@click.argument('username')
@click.argument('zonename')
def usermod_zone_add(username, zonename):
	try:
		add_zoneacl(username, zonename)
	except KnotRestUserError as e:
		click.echo(f"User '{e.username}' doesn't exist!")
		exit(1)
	except:
		click.echo(f"Unknown error")
		exit(1)
	click.echo("Success")

@zone_cli.command("del", help="Delete user rights to the zone")
@click.argument('username')
@click.argument('zonename')
def usermod_zone_del(username, zonename):
	try:
		del_zoneacl(username, zonename)
	except KnotRestUserError as e:
		click.echo(f"User '{e.username}' doesn't exist!")
		exit(1)
	except:
		click.echo(f"Unknown error")
		exit(1)
	click.echo("Success")

bp.cli.add_command(zone_cli)


admin_cli = AppGroup('admin')
admin_cli.short_help = "Modify user admin rights."
admin_cli.help = "This group of commands allows modify user admin rights."

@admin_cli.command("add", help="Add user admin rights")
@click.argument('username')
def usermod_admin_add(username):
	try:
		add_admin(username)
	except KnotRestUserError as e:
		click.echo(f"User '{e.username}' doesn't exist!")
		exit(1)
	except:
		click.echo(f"Unknown error")
		exit(1)
	click.echo("Success")

@admin_cli.command("del", help="Remove user admin rights")
@click.argument('username')
def usermod_admin_del(username):
	try:
		del_admin(username)
	except KnotRestUserError as e:
		click.echo(f"User '{e.username}' doesn't exist!")
		exit(1)
	except:
		click.echo(f"Unknown error")
		exit(1)
	click.echo("Success")

bp.cli.add_command(admin_cli)

description_cli = AppGroup('description')
description_cli.short_help = "Access user descrition."
description_cli.help = "This group of commands allows access user description."

@description_cli.command("set", help="Set user description")
@click.argument('username')
@click.argument('description')
def usermod_description_set(username, description):
	try:
		set_description(username, description)
	except KnotRestUserError as e:
		click.echo(f"User '{e.username}' doesn't exist!")
		exit(1)
	except:
		click.echo(f"Unknown error")
		exit(1)
	click.echo("Success")

bp.cli.add_command(description_cli)