from datetime import datetime
from functools import wraps
from flask import abort
from libknot.dname import KnotDname
from sqlalchemy.exc import NoResultFound, IntegrityError

from knot_rest.database import db
from knot_rest.exceptions import KnotRestError

class ZoneLock(db.Model):
	zone = db.Column(db.LargeBinary(KnotDname.CAPACITY), primary_key = True)
	created = db.Column(db.DateTime(timezone=True))

	def __init__(self, zone):
		self.zone = KnotDname(dname_str=zone).wire()
		self.created = datetime.now()

def zonelock_disable(zonename):
	try:
		dn = KnotDname(dname_str=zonename)
		zl = ZoneLock.query.filter(ZoneLock.zone == dn.wire()).one()
		db.session.delete(zl)
		db.session.commit()
	except NoResultFound:
		raise KnotRestError(f"ZoneLock '{zonename}' doesn't exists!")

def zonelock_enable(zonename):
	try:
		lock = ZoneLock(zonename)
		db.session.add(lock)
		db.session.commit()
	except IntegrityError:
		db.session.rollback()
		raise KnotRestError(f"ZoneLock '{zonename}' already exists!")

def zonelock_list():
	zls = ZoneLock.query.filter().all()
	return zls

def zonelock_is_locked(zonename):
	try:
		dn = KnotDname(dname_str=zonename)
		ZoneLock.query.filter(ZoneLock.zone == dn.wire()).one()
		return True
	except NoResultFound:
		return False