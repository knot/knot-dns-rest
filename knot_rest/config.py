from collections.abc import Mapping
from flask import Config, Flask
from os.path import abspath, exists, join
from secrets import token_hex
from schema import  Optional, Schema, SchemaError, And, Or
from yaml import CLoader, load

from knot_rest.exceptions import KnotRestConfigError

import os

def ValPath(**kwargs):
	return Val(And(str, os.path.exists, **kwargs))

def Val(*args):
	return Or(None, *args)

_SCHEMA = Schema({
	Optional('audit-log'): Val(str),
	Optional('database'): Val(str),
	Optional('default-ttl'): Val(int),
	Optional('libknot'): ValPath(error='Invalid \'libknot.so\' path'),
	Optional('socket'): {
		Optional('path'): Val(str),
		Optional('timeout'): Val(int)
	},
	Optional('token'): {
		Optional('algorithm'): Val(str),
		Optional('expiration'): Val(int),
		Optional('secret'): Val(str),
	}
})

_DEFAULTS = {
	'audit-log': None,
	'database': 'sqlite:////var/lib/knot_rest/database.db',
	'default-ttl': None,
	'libknot': None,
	'socket': {
		'path': '/var/run/knot/knot.sock',
		'timeout': None
	},
	'token': {
		'algorithm': 'HS256',
		'expiration': None,
		'secret': token_hex()

	}
}

def parse_file(config : Config, filename : str) -> bool:
	filename = join(config.root_path, filename)

	try:
		with open(filename) as f:
			obj = load(f, Loader=CLoader)
	except OSError as e:
		raise KnotRestConfigError(f"Unable to load file ({e.strerror})")

	if obj is None:
		return {}

	return obj

def update(dict, new):
	if not isinstance(dict, Mapping):
		dict = {}
	for k, v in new.items():
		if isinstance(v, Mapping):
			dict[k] = update(dict.get(k, {}), v)
		else:
			dict[k] = v
	return dict


def load_config(app : Flask, specified_files = ['/etc/knot_rest/knot_rest.yaml', './knot_rest.yaml']):
	for f in specified_files:
		if exists(f):
			break
	else:
		return False

	config = update(None, _DEFAULTS)
	file_config = parse_file(app.config, abspath(f))
	update(config, file_config)

	try:
		_SCHEMA.validate(config)
	except SchemaError as e:
		if (e.errors[-1]):
			raise KnotRestConfigError(e.errors[-1])
		raise KnotRestConfigError(e.autos[-1])

	app.config.update(config)

	return True