from flask import abort, current_app, request
from flask_httpauth import HTTPTokenAuth
from libknot.dname import KnotDname
from time import time
from secrets import token_hex
from sqlalchemy.exc import IntegrityError, NoResultFound, OperationalError
from sqlalchemy.sql import func
from werkzeug.security import generate_password_hash, check_password_hash

import jwt

from knot_rest.database import db
from knot_rest.exceptions import KnotRestUserError
from knot_rest.shared import get_argument

auth = HTTPTokenAuth('Bearer')

class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	pub_id = db.Column(db.String, unique=True, nullable=False, index=True)
	username = db.Column(db.String, unique=True, nullable=False)
	password_hash = db.Column(db.String, nullable=False)
	admin = db.Column(db.Boolean, nullable=False)
	zoneacl = db.relationship("ZoneACL", cascade="all,delete", backref="User")
	description = db.Column(db.String, nullable=True)
	created = db.Column(db.DateTime(timezone=True), server_default=func.now())
	updated = db.Column(db.DateTime(timezone=True), onupdate=func.now())
	logged_in = db.Column(db.DateTime(timezone=True))

	def __init__(self, username, password, admin = False, description = None):
		self.pub_id = token_hex()
		self.username = username
		self.password_hash = generate_password_hash(password)
		self.admin = admin
		self.description = description

class ZoneACL(db.Model):
	__tablename__ = 'zone_acl'
	user = db.Column(db.ForeignKey(User.id), primary_key = True)
	zone = db.Column(db.LargeBinary(KnotDname.CAPACITY), primary_key = True)

	def __init__(self, user, zone):
		self.user = user
		self.zone = zone

@auth.get_user_roles
def get_user_roles(user):
	if user.admin:
		return ['zone', 'users', 'control']

	if 'zone' in request.view_args:
		zone = request.view_args['zone']
	else:
		zone = get_argument('zone')
	if not zone:
		abort(403, "Forbidden")

	dn = KnotDname(dname_str=zone)
	try:
		ZoneACL.query.filter(ZoneACL.user == user.id, ZoneACL.zone == dn.wire()).one()
		return ['zone']
	except NoResultFound:
		abort(403, "Forbidden")
	except OperationalError:
		abort(500, "Database error")

@auth.verify_token
def verify_token(token):
	try:
		decoded = jwt.decode(token, current_app.config['token']['secret'], algorithms='HS256')
		user = User.query.filter(User.pub_id == decoded['id']).one()
		return user
	except jwt.DecodeError:
		abort(401, "Wrong token")
	except jwt.ExpiredSignatureError:
		abort(401, "Token expired")
	except NoResultFound:
		abort(401, "User not found")
	except Exception:
		abort(500)

def register_user(uname, passwd, description = None):
	nbytes = 16
	user = User(username=uname, password=passwd, description=description)
	while True:
		try:
			db.session.add(user)
			db.session.commit()
			break
		except IntegrityError as e:
			if e.orig.args[0] == 'UNIQUE constraint failed: user.username':
				db.session.rollback()
				raise KnotRestUserError(uname, f"User '{uname}' already exists!")
			elif e.orig.args[0] == 'UNIQUE constraint failed: user.pub_id':
				nbytes += 1
				user.pub_id = token_hex(nbytes=nbytes)
			else:
				raise
		except Exception:
			raise

def reset_password(uname, passwd):
	try:
		user = User.query.filter(User.username == uname).one()
		user.password_hash = generate_password_hash(passwd)
		db.session.commit()
	except NoResultFound:
		raise KnotRestUserError(uname, f"User '{uname}' doesn't exists!")
	except Exception:
		raise

def remove_user(uname):
	try:
		user = User.query.filter(User.username == uname).one()
		db.session.delete(user)
		db.session.commit()
	except NoResultFound:
		raise KnotRestUserError(uname, f"User '{uname}' doesn't exists!")
	except Exception:
		raise

def list_users():
	try:
		return [{
			'username': user.username,
			'admin': user.admin,
			'description': user.description,
			'zones': [ KnotDname(dname_wire=zone.zone).str() for zone in user.zoneacl ]
		} for user in User.query.all()]
	except NoResultFound:
		raise KnotRestUserError(str=f"No users exists!")
	except Exception:
		raise

def user_info(uname):
	try:
		user = User.query.filter(User.username == uname).one()

		return {
			'username': user.username,
			'admin': user.admin,
			'description': user.description,
			'created': user.created,
			'updated': user.updated,
			'logged_in': user.logged_in,
			'zones': [ KnotDname(dname_wire=zone.zone).str() for zone in user.zoneacl ]
		}
	except NoResultFound:
		raise KnotRestUserError(uname, f"User not exist!")
	except Exception:
		raise

def generate_auth_token(pub_id, expires_in = 600):
	return jwt.encode({
			'id': pub_id,
			'exp': time() + expires_in
		}, current_app.config['token']['secret'], algorithm='HS256')

def generate_token(uname, passwd):
	try:
		user = User.query.filter(User.username == uname).one()
	except NoResultFound:
		abort(401, "Invalid login credentials")

	if not check_password_hash(user.password_hash, passwd):
		abort(401, "Invalid login credentials")

	if current_app.config['token']['expiration']:
		token = generate_auth_token(user.pub_id, current_app.config['token']['expiration'])
	else:
		token = generate_auth_token(user.pub_id)

	user.logged_in = func.now()
	db.session.commit()

	return {
			'token': token,
			'duration': current_app.config['token']['expiration']
		}

def add_admin(uname):
	try:
		user = User.query.filter(User.username == uname).one()
		if user.admin:
			return
		user.admin = True
		db.session.commit()
	except NoResultFound:
		raise KnotRestUserError(uname, f"User \'{uname}\' does not exist!")

def del_admin(uname):
	try:
		user = User.query.filter(User.username == uname).one()
		if not user.admin:
			return
		user.admin = False
		db.session.commit()
	except NoResultFound:
		raise KnotRestUserError(uname, f"User \'{uname}\' does not exist!")

def add_zoneacl(uname, zonename):
	try:
		user = User.query.filter(User.username == uname).one()
	except NoResultFound:
		raise KnotRestUserError(uname, f"User {uname} does not exist!")

	dname = KnotDname(dname_str=zonename)
	try:
		ZoneACL.query.filter(ZoneACL.user == user.id, ZoneACL.zone == dname.wire()).one()
	except NoResultFound:
		try:
			zoneacl = ZoneACL(user.id, dname.wire())
			db.session.add(zoneacl)
			db.session.commit()
		except IntegrityError:
			db.session.rollback()
			return -1
	return 0

def del_zoneacl(uname, zonename):
	try:
		user = User.query.filter(User.username == uname).one()
	except NoResultFound:
		raise KnotRestUserError(uname, f"User {uname} does not exist!")

	dname = KnotDname(dname_str=zonename)
	try:
		zoneacl = ZoneACL.query.filter(ZoneACL.user == user.id, ZoneACL.zone == dname.wire()).one()
		db.session.delete(zoneacl)
		db.session.commit()
	except NoResultFound:
		return 0
	except:
		db.session.rollback()
		raise
	return 0

def set_description(uname, description):
	try:
		user = User.query.filter(User.username == uname).one()
		user.description = description
		db.session.commit()
	except NoResultFound:
		raise KnotRestUserError(uname, f"User \'{uname}\' does not exist!")
