from flask import Flask
from libknot import Knot, control as knot
from logging import Formatter, getLogger
from logging.config import dictConfig
from logging.handlers import TimedRotatingFileHandler

from knot_rest import usermod
from knot_rest.config import _DEFAULTS, load_config, update
from knot_rest.database import bp as db_bp, db, migration
from knot_rest.exceptions import KnotRestConfigError
from knot_rest.resources import control, error, records, service, users, zones

# Setup logger
dictConfig({
	'version': 1,
	'formatters': {
		'default': {
			'format': '[%(asctime)s] %(levelname)s: %(message)s',
		}
	},
	'handlers': {
		'wsgi': {
			'class': 'logging.StreamHandler',
			'stream': 'ext://flask.logging.wsgi_errors_stream',
			'formatter': 'default',
		}
	},
	'root': {
		'level': 'INFO',
		'handlers': ['wsgi']
	},
	'loggers': {
		'audit': {
			'level': 'DEBUG'
		}
	}
})

def audit_handler_factory(path: str):
	handler = TimedRotatingFileHandler(path, when="midnight")
	handler.setFormatter(Formatter('[%(asctime)s]: %(message)s'))
	handler.suffix = "%Y%m%d"
	return handler

def create_app(config = None, test_config = None) -> Flask:
	# Init framework
	app = Flask(__name__)

	# Parse configuration
	if not test_config:
		try:
			if config:
				if not load_config(app, [config]):
					app.logger.error(f"Configuration file '{config}' not found")
					return None
			else:
				load_config(app)
		except KnotRestConfigError as e:
			app.logger.error(e.strerror)
			return None
	else:
		config = update(None, _DEFAULTS)
		update(config, test_config)
		app.config.update(config)
		app.config['TESTING'] = True

	# Connect to database
	app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
	app.config['SQLALCHEMY_DATABASE_URI'] = app.config['database']
	db.init_app(app)
	migration.init_app(app)

	# Setup audit logging handler
	## Delete old handlers stored when using 'pytest'
	audit_logger = getLogger('audit')
	for h in audit_logger.handlers:
		audit_logger.removeHandler(h)
	## Add new handler
	if (app.config['audit-log']):
		try:
			audit_logger.addHandler(audit_handler_factory(app.config['audit-log']))
		except Exception as e:
			app.logger.warning(f"Unable to access '{app.config['audit-log']}' file for audit log")

	# Store app context
	app.app_context().push()

	# Load shared object
	try:
		app.logger.debug("Loading 'libknot.so'")
		Knot(app.config['libknot'])
	except OSError:
		app.logger.error('Unable to find Knot library on %s', app.config['libknot'] if app.config['libknot'] else 'system library path')
		return None

	# Test right socket path
	try:
		app.logger.debug("Testing Knot DNS control socket connection")
		ctl = knot.KnotCtl()
		ctl.connect(app.config['socket']['path'])
		ctl.send_block(cmd="status")
		ctl.receive_block()
		ctl.close()
	except knot.KnotCtlErrorConnect:
		app.logger.warning('Unable to connect to Knot socket on path %s - Knot DNS related commands will not work', app.config['socket']['path'])

	# Print deprecated configuration values
	if app.config['default-ttl']:
		app.logger.warning("Deprecated 'default-ttl' entry in configuration!")

	# Register error handlers
	error.register_error_handlers(app)

	# Register cli commands
	app.register_blueprint(db_bp)
	app.register_blueprint(service.bp)
	app.register_blueprint(usermod.bp)

	# Register resources
	app.register_blueprint(control.bp)
	app.register_blueprint(records.bp)
	app.register_blueprint(users.bp)
	app.register_blueprint(zones.bp)

	return app