from flask import Blueprint, abort, jsonify
from libknot import control as knot
from werkzeug.exceptions import HTTPException

from knot_rest.auth import auth
from knot_rest.resources.records import Record, map_to_record_list
from knot_rest.shared import get_argument
from knot_rest.socket import open_socket

bp = Blueprint('zones', __name__)

class Zone:
	def __init__(self, name : str = None, serial : int = None, records : list = None):
		self.name = name
		self.serial = serial
		self.records = records

	@staticmethod
	def factory(el : dict):
		zone = el[0]
		serial = el[1][zone]['SOA']['data'][0].split()[2]
		records = map_to_record_list({el[0]:el[1]})
		return Zone(zone, serial, records)

	def serialize(self):
		return {
			"name": self.name,
			"url": "/".join(["/zones", self.name]),
			"serial": self.serial,
			"records": list(map(Record.serialize, self.records))
		}

# List all (or specified) zones on a server.
#
# Parameters:
#	zone (optional): Zone name
#
# Query Parameters:
# 	zone: Zone name
#
# Status codes:
# 	200 OK - An array of zones
# 	404 Not Found - Requested item was not found
#	500 Internal Server Error - The server has encountered a situation it does not know how to handle
@bp.route('/zones', methods=['GET'])
@bp.route('/zones/<zone>', methods=['GET'])
@auth.login_required(role = ['zone'])
def zones_get(zone = None):
	# Get arguments
	if not zone:
		zone = get_argument('zone')

	# Get data
	try:
		with open_socket() as ctl:
			ctl.send_block(cmd="zone-read", zone=zone)
			resp = ctl.receive_block()
	except knot.KnotCtlError as e:
		if e.message == 'no such zone found':
			abort(404, 'Zone not found')
		if e.message == 'parser failed':
			abort(400, 'Invalid parameter')
		abort(500)
	except HTTPException:
		raise
	except:
		abort(500)

	if not resp:
		abort(404, "Zone not found")
	
	# Serialize
	zones = list(map(lambda el: Zone.factory(el).serialize(), resp.items()))
	return jsonify(zones)