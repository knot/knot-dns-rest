from flask import Blueprint
from libknot.dname import KnotDname
from sys import exit

import click

from knot_rest.exceptions import KnotRestError
from knot_rest.socket import open_socket
from knot_rest.zonelock import zonelock_disable, zonelock_enable, zonelock_list

bp = Blueprint('service', __name__)
bp.cli.short_help = "Service manipulation."
bp.cli.help = "This group of commands allows standardized manipulation of the daemon services."

# Cli
@bp.cli.command("disable", help="Disables zone operations.")
@click.argument('zonename')
@click.option('-f', '--force', is_flag=True)
def disable_zone(zonename, force):
	try:

		if not force:
			with open_socket() as ctl:
				ctl.send_block(cmd="zone-read", zone=zonename)
				ctl.receive_block()
		zonelock_enable(zonename)
	except KnotRestError as e:
		click.echo(e.strerror)
		exit(1)
	click.echo("Success")

@bp.cli.command("enable", help="Enables zone operations.")
@click.argument('zonename')
def enable_zone(zonename):
	try:
		zonelock_disable(zonename)
	except KnotRestError as e:
		click.echo(e.strerror)
		exit(1)
	click.echo("Success")

@bp.cli.command("status", help="Prints service status.")
@click.option('-v', '--verbose', is_flag=True)
def status(verbose):
	with open_socket() as ctl:
		ctl.send_block(cmd="status")
		ctl.receive_block()
		click.echo("Knot DNS:\tCONNECTED")
	zls = zonelock_list()
	if verbose:
		click.echo(f"Locked zones:")
		for zl in zls:
			dn = KnotDname(dname_wire=zl.zone)
			click.echo(f"\t{dn.str()}\t{zl.created}")
	else:
		click.echo(f"Locked zones:\t{len(zls)}")