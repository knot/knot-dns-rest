
from enum import IntEnum
from flask import Blueprint, request

from knot_rest.auth import auth
from knot_rest.exceptions import KnotRestError
from knot_rest.shared import get_argument
from knot_rest.socket import open_socket

from libknot import control as knot
from schema import  Optional, Or, Schema

bp = Blueprint('control', __name__)

class Flags(IntEnum):
	MANDATORY_ARG  = (1 << 0) # Needs at least one zone
	MANDATORY_DIR  = (1 << 1) # Needs 'd' filter
	ALLOW_BLOCKING = (1 << 2) # Allow blocking
	ALLOW_FORCE    = (1 << 3) # Allow force

def filter_set(input = None):
	out = set()
	if input:
		out.update(input)
	return out

_COMMAND_TABLE = {
	"status":        ("zone-status",        filter_set('cefrst'),          0 ),
	"reload":        ("zone-reload",        filter_set(),                  Flags.ALLOW_BLOCKING | Flags.ALLOW_FORCE ),
	"refresh":       ("zone-refresh",       filter_set(),                  Flags.ALLOW_BLOCKING ),
	"notify":        ("zone-notify",        filter_set(),                  Flags.ALLOW_BLOCKING ),
	"retransfer":    ("zone-retransfer",    filter_set(),                  Flags.ALLOW_BLOCKING ),
	"flush":         ("zone-flush",         filter_set('d'),               Flags.ALLOW_BLOCKING ),
	"backup":        ("zone-backup",        filter_set('cdjkotqzCJKOTQZ'), Flags.MANDATORY_DIR | Flags.ALLOW_BLOCKING | Flags.ALLOW_FORCE ),
	"restore":       ("zone-restore",       filter_set('d'),               Flags.MANDATORY_DIR | Flags.ALLOW_BLOCKING | Flags.ALLOW_FORCE ),
	"sign":          ("zone-sign",          filter_set(),                  Flags.ALLOW_BLOCKING ),
	"validate":      ("zone-validate",      filter_set(),                  Flags.ALLOW_BLOCKING ),
	"keys-load":     ("zone-keys-load",     filter_set(),                  Flags.ALLOW_BLOCKING ),
	# "key-rollover" have been moved to own function due to different syntax
	"ksk-submitted": ("zone-ksk-submitted", filter_set(),                  Flags.MANDATORY_ARG | Flags.MANDATORY_DIR | Flags.ALLOW_BLOCKING ),
	"freeze":        ("zone-freeze",        filter_set(),                  Flags.ALLOW_BLOCKING ),
	"thaw":          ("zone-thaw",          filter_set(),                  Flags.ALLOW_BLOCKING ),
	"xfr-freeze":    ("zone-xfr-freeze",    filter_set(),                  Flags.ALLOW_BLOCKING ),
	"xfr-thaw":      ("zone-xfr-thaw",      filter_set(),                  Flags.ALLOW_BLOCKING ),
}

def convert_dict_defaults_to_none(input):
	if not isinstance(input, dict):
		return input

	return {
		key: convert_dict_defaults_to_none(value)
		if isinstance(value, dict)
		else (None if value == '-' else value)
		for key, value in input.items()
	}

def control_zone(cmd, zones, filter_str, flags_str, data):
	query = knot.KnotCtlData()
	query[knot.KnotCtlDataIdx.COMMAND] = cmd
	query[knot.KnotCtlDataIdx.FLAGS]   = flags_str
	query[knot.KnotCtlDataIdx.DATA]    = data
	query[knot.KnotCtlDataIdx.FILTER]  = filter_str
	with open_socket() as ctl:
		for zone in zones:
			query[knot.KnotCtlDataIdx.ZONE] = zone
			ctl.send(knot.KnotCtlType.DATA, data=query)
		ctl.send(knot.KnotCtlType.BLOCK, None)
		resp = ctl.receive_block()
		return convert_dict_defaults_to_none(resp)
	return {}

@bp.route('/control/zone/<command>', methods=['GET'])
@bp.route('/control/zone/<command>/<zone>', methods=['GET'])
@auth.login_required(role = ['control'])
def control_zone_get(command, zone = None):
	if command not in _COMMAND_TABLE:
		KnotRestError('The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again.', 404).abort()

	(cmd, cmd_filters, cmd_flags) = _COMMAND_TABLE[command]
	if cmd_flags & Flags.MANDATORY_ARG and not zone:
		KnotRestError('Missing argument', 400).abort()

	filters = filter_set(get_argument('filters'))
	flags = filter_set(get_argument('flags'))
	data = get_argument('dir')
	if data:
		filters.update('d')
	elif cmd_flags & Flags.MANDATORY_DIR: # and data == None
		KnotRestError('Missing output directory', 400).abort()

	# Test received filters
	if len(filters - cmd_filters):
		KnotRestError('Unsupported filter', 400).abort()

	# Test flags
	flags_str = ''.join(flags) if len(flags) else None
	if Flags.ALLOW_BLOCKING and 'B' in flags:
		flags.remove('B')
	if Flags.ALLOW_FORCE and 'F' in flags:
		flags.remove('F')
	if len(flags):
		KnotRestError('Unsupported flags', 400).abort()

	try:
		filter_str = ''.join(filters) if len(filters) else None
		return control_zone(cmd, [zone], filter_str, flags_str, data)
	except knot.KnotCtlErrorRemote as e:
		if e.message == 'no such zone found':
			KnotRestError('No zone found', 404).abort()
		if e.message == 'missing parameter':
			KnotRestError('Missing parameter', 400).abort()
		if e.message == 'operation failed for some zones':
			KnotRestError('Operation failed for some zones', 500).abort()
		if e.message == 'operation not permitted':
			KnotRestError('Operation not permited', 403).abort()
		if e.message == 'already exists':
			KnotRestError('Already exists', 409).abort()
		if e.message == 'requested resource is busy':
			KnotRestError('Backend is busy', 500).abort()
		if e.message == 'operation not supported':
			KnotRestError('Operation not supported', 400).abort()
		if e.message == 'invalid parameter':
			KnotRestError('Invalid parameter', 400).abort()
		raise
	except KnotRestError as e:
		e.abort()
	except:
		raise


_CONTROL_ZONE_POST_SCHEMA = Schema({
	Optional("filters"): Or(str, None),
	Optional("flags"): Or(str, None),
	Optional("dir"): Or(str, None),
	"zones": Or(None, [
		Optional(str)
	])
})

@bp.route('/control/zone/<command>', methods=['POST'])
@auth.login_required(role = ['control'])
def control_zone_post(command):
	if not request.is_json:
		KnotRestError('Not JSON request', 400).abort()

	operations = request.get_json()

	if command not in _COMMAND_TABLE:
		KnotRestError('The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again.', 404).abort()

	try:
		_CONTROL_ZONE_POST_SCHEMA.validate(operations)
	except:
		KnotRestError('Invalid request format', 400).abort()

	(cmd, cmd_filters, cmd_flags) = _COMMAND_TABLE[command]
	zones = operations['zones']
	filters = None
	flags = None
	data = None
	if "filters" in operations:
		filters = filter_set(operations['filters'])
	if "flags" in operations:
		flags = filter_set(operations['flags'])
	if "dir" in operations:
		data = operations['dir']

	if cmd_flags & Flags.MANDATORY_ARG and len(zones) == 0:
		KnotRestError('Missing argument', 400).abort()

	if filters == None:
		filters = filter_set(get_argument('filters'))
	if flags == None:
		flags = filter_set(get_argument('flags'))
	if data == None:
		data = get_argument('dir')

	if data:
		filters.update('d')
	elif cmd_flags & Flags.MANDATORY_DIR: # and data == None
		KnotRestError('Missing output directory', 400).abort()

	# Test received filters
	if len(filters - cmd_filters):
		KnotRestError('Unsupported filter', 400).abort()

	# Test received flags
	flags_str = ''.join(flags) if len(flags) else None
	if Flags.ALLOW_BLOCKING and 'B' in flags:
		flags.remove('B')
	if Flags.ALLOW_FORCE and 'F' in flags:
		flags.remove('F')
	if len(flags):
		KnotRestError('Unsupported flags', 400).abort()

	try:
		filter_str = ''.join(filters) if len(filters) else None
		return control_zone(cmd, zones, filter_str, flags_str, data)
	except knot.KnotCtlErrorRemote as e:
		if e.message == 'no such zone found':
			KnotRestError('No zone found', 404).abort()
		if e.message == 'missing parameter':
			KnotRestError('Missing parameter', 400).abort()
		if e.message == 'operation failed for some zones':
			KnotRestError('Operation failed for some zones', 500).abort()
		if e.message == 'operation not permitted':
			KnotRestError('Operation not permited', 403).abort()
		if e.message == 'already exists':
			KnotRestError('Already exists', 409).abort()
		if e.message == 'requested resource is busy':
			KnotRestError('Backend is busy', 500).abort()
		if e.message == 'operation not supported':
			KnotRestError('Operation not supported', 400).abort()
		if e.message == 'not exists':
			KnotRestError('Not exists', 404).abort()
		raise
	except KnotRestError as e:
		e.abort()
	except:
		raise


@bp.route('/control/zone/key-rollover/<zone>/<keytype>', methods=['GET'])
@auth.login_required(role = ['control'])
def control_zone_key_rollover(zone, keytype):
	if keytype != 'ksk' or keytype != 'zsk':
		KnotRestError('Unsupported keytype', 400).abort()
	flags = filter_set(get_argument('flags'))
	if len(flags - filter_set('B')) > 0:
		KnotRestError('Unsupported flags', 400).abort()
	flags_str = ''.join(flags) if len(flags) else None
	try:
		with open_socket() as ctl:
			ctl.send_block(cmd='zone-key-rollover', zone=zone, rtype=keytype, flags=flags_str)
			resp = ctl.receive_block()
			return convert_dict_defaults_to_none(resp)
	except knot.KnotCtlErrorRemote as e:
		if e.message == 'no such zone found':
			KnotRestError('No zone found', 404).abort()
		if e.message == 'missing parameter':
			KnotRestError('Missing parameter', 400).abort()
		if e.message == 'operation failed for some zones':
			KnotRestError('Operation failed for some zones', 500).abort()
		if e.message == 'operation not permitted':
			KnotRestError('Operation not permited', 403).abort()
		if e.message == 'already exists':
			KnotRestError('Already exists', 409).abort()
		if e.message == 'requested resource is busy':
			KnotRestError('Backend is busy', 500).abort()
		if e.message == 'operation not supported':
			KnotRestError('Operation not supported', 400).abort()
		if e.message == 'not exists':
			KnotRestError('Not exists', 404).abort()
		raise
	except KnotRestError as e:
		e.abort()
	except:
		raise