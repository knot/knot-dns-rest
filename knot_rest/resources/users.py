from logging import getLogger
from sys import exit
from flask import Blueprint, abort, g, request

import click

from knot_rest.auth import generate_token, list_users, register_user, remove_user, reset_password, user_info, auth
from knot_rest.exceptions import KnotRestUserError
from knot_rest.shared import get_argument

bp = Blueprint('users', __name__)
bp.cli.short_help = "User manipulation."
bp.cli.help = "This group of commands allows standardized manipulation of the users."

@bp.route('/user/register', methods=['POST'])
@auth.login_required(role=['users'])
def user_create():
	username = get_argument('username')
	password = get_argument('password')
	description = get_argument('description')
	if not username:
		abort(400, "Missing username")
	if not password:
		abort(400, "Missing password")

	try:
		register_user(username, password, description)
		getLogger('audit').info(f"User '{g.flask_httpauth_user.username}' has registered user '{username}'")
		return {
			'status': 'success',
			'username': username
		}
	except KnotRestUserError as e:
		getLogger('audit').info(f"User '{g.flask_httpauth_user.username}' failed to register user '{username}'")
		abort(409, e.strerror)

@bp.route('/user/login')
def get_token():
	if not request.authorization:
		abort(401, "Missing login credentials")
	if not request.authorization.username:
		abort(401, "Missing username")

	uname = request.authorization.username
	passwd = request.authorization.password

	try:
		token = generate_token(uname, passwd)
		getLogger('audit').info(f"User '{uname}' logged in")
		return token
	except:
		getLogger('audit').info(f"User '{uname}' failed to log in")
		raise

@bp.route('/user/info', methods=['GET'])
@auth.login_required()
def get_user_info():
	try:
		return user_info(g.flask_httpauth_user.username)
	except KnotRestUserError as e:
		abort(409, e.strerror)


@bp.route('/user/info/<user>', methods=['GET'])
@auth.login_required(role = ['users'])
def get_specific_user_info(user):
	if not user:
		user = get_argument('user')
	try:
		return user_info(user)
	except KnotRestUserError as e:
		abort(409, e.strerror)

# Cli
@bp.cli.command("add", help="Create new user in database.")
@click.argument('username')
@click.argument('password')
@click.argument('description', required = False)
def users_add(username, password, description = None):
	try:
		register_user(username, password, description)
		getLogger('audit').info(f"Registered user '{username}' from CLI")
		click.echo("Success")
	except KnotRestUserError as e:
		click.echo(e.strerror)
		exit(1)
	except:
		click.echo("Unknown error")
		exit(1)

@bp.cli.command("reset", help="Set users new password.")
@click.argument('username')
@click.argument('password')
def users_reset(username, password):
	try:
		reset_password(username, password)
		getLogger('audit').info(f"Reset password of '{username}' to new password from CLI")
		click.echo("Success")
	except KnotRestUserError as e:
		click.echo(e.strerror)
		exit(1)
	except:
		click.echo("Unknown error")
		exit(1)

@bp.cli.command("remove", help="Remove user from database.")
@click.argument('username')
def users_remove(username):
	try:
		remove_user(username)
		getLogger('audit').info(f"Removed user '{username}' from CLI")
		click.echo("Success")
	except KnotRestUserError as e:
		click.echo(e.strerror)
		exit(1)
	except:
		click.echo("Unknown error")
		exit(1)

@bp.cli.command("list", help="List users from database.")
def users_list():
	try:
		users = list_users()
		click.echo(users)
	except KnotRestUserError as e:
		click.echo(e.strerror)
		exit(1)