from flask import Blueprint, abort, jsonify, current_app, g, request
from libknot import control as knot
from logging import getLogger
from urllib.parse import quote, unquote
from werkzeug.exceptions import HTTPException

from knot_rest.auth import auth
from knot_rest.exceptions import KnotRestError, KnotRestOperationError
from knot_rest.shared import get_argument
from knot_rest.socket import open_socket, zone_transaction
from knot_rest.zonelock import zonelock_is_locked

bp = Blueprint('records', __name__)

class Record:
	def __init__(self, zone, name, rtype, ttl, data):
		self.zone = zone
		self.name = name
		self.rtype = rtype
		self.ttl = ttl
		self.data = data

	def __lt__(self, other) -> bool:
		if self.rtype == 'SOA' and other.rtype != 'SOA':
			return True
		elif self.rtype != 'SOA' and other.rtype == 'SOA':
			return False
		if self.zone != other.zone:
			return self.zone < other.zone
		if self.name != other.name:
			return self.name < other.name
		if self.rtype != other.rtype:
			return self.rtype < other.rtype
		if self.data != other.data:
			return self.data < other.data
		return self.ttl < other.ttl

	def __eq__(self, other) -> bool:
		return self.zone == other.zone \
			and self.name == other.name \
			and self.ttl == other.ttl \
			and self.data == other.data

	def __hash__(self) -> int:
		return hash((self.zone, self.name, self.rtype, self.ttl, self.data))

	def serialize(self):
		return {
			"url": "/".join(["/zones", self.zone, "records", self.name, self.rtype, quote(self.data)]),
			"name": self.name,
			"ttl": self.ttl,
			"rtype": self.rtype,
			"data": self.data
		}

def map_to_record_list(map : dict):
	out = list()
	for (zone, rest1) in map.items():
		for (name, rest2) in rest1.items():
			for (rtype, rest3) in rest2.items():
				ttl = rest3['ttl']
				for data in rest3['data']:
					out.append(Record(zone, name, rtype, ttl, data))
	return out

def list_diff(old, new):
	old_set = set(old)
	new_set = set(new)
	added = new_set - old_set
	removed = old_set - new_set
	return sorted(removed), sorted(added)

def diff_to_string(removed, added):
	out = ""
	for r in removed:
		if len(out):
			out += '\n'
		out += f"  -\t{r.name}\t{r.ttl}\t{r.rtype}\t{r.data}"
	for r in added:
		if len(out):
			out += '\n'
		out += f"  +\t{r.name}\t{r.ttl}\t{r.rtype}\t{r.data}"
	return out

def _records_get(ctl, zone, name, rtype, ttl, data):
	try:
		if ttl != None:
			ttl = str(ttl)
		ctl.send_block(cmd="zone-get", zone=zone, owner=name, rtype=rtype)
		resp = ctl.receive_block()
		records = map_to_record_list(resp)
		if ttl:
			records = filter(lambda r: r.ttl == ttl, records)
		if data:
			records = filter(lambda r: r.data == data, records)
		records = list(records)
		if not records:
			raise KnotRestError('No record found', 404)
		return records
	except knot.KnotCtlErrorRemote as e:
		if e.message == 'no such node in zone found':
			raise KnotRestError('No record found', 404)
		raise

# List all (or specified) records in a zone.
#
# Parameters:
#	zone (mandatory): Zone name
#	name (optional): Record domain name
#	rtype (optional): Record type
#	data (optional): Percent encoded (URL encoded) record data
#	ttl (optional): Record set TTL
#
# Query Parameters:
#	name: Record domain name
#	ttl: Record set TTL
#	rtype: Record type
#	data: Percent encoded (URL encoded) record data
#
# Status codes:
# 	200 OK - An array of zones
# 	404 Not Found - Requested item was not found
#	500 Internal Server Error - The server has encountered a situation it does not know how to handle
@bp.route('/zones/<zone>/records', methods=['GET'])
@bp.route('/zones/<zone>/records/<name>', methods=['GET'])
@bp.route('/zones/<zone>/records/<name>/<rtype>', methods=['GET'])
@bp.route('/zones/<zone>/records/<name>/<rtype>/<data>', methods=['GET'])
@bp.route('/zones/<zone>/records/<name>/<rtype>/<data>/<ttl>', methods=['GET'])
@auth.login_required(role = ['zone'])
def records_get(zone, name = None, rtype = None, data = None, ttl = None):
	# Get arguments
	if not name:
		name = get_argument('name')
	if not ttl:
		ttl = get_argument('ttl')
	if not rtype:
		rtype = get_argument('rtype')
	if not data:
		data = get_argument('data')

	# URL encoding
	if name:
		name = unquote(name)
	if data:
		data = unquote(data)

	# Get data
	try:
		with open_socket() as ctl, zone_transaction(ctl, zone):
			records = _records_get(ctl, zone, name, rtype, ttl, data)
		if not records:
			abort(404, 'No record found')
		return jsonify(list(map(Record.serialize, records)))
	except KnotRestError as e:
		e.abort()
	except knot.KnotCtlErrorRemote as e:
		if e.message == 'no such node in zone found':
			abort(404, 'No record found')
		if e.message == 'invalid parameter':
			abort(400, 'Invalid parameter')
		if e.message == 'no such zone found':
			abort(404, 'No such zone found')
		abort(500)
	except HTTPException:
		raise
	except:
		abort(500)

def _records_put(ctl, zone, name, rtype, ttl, data):
	try:
		if not name or not rtype or not data:
			raise KnotRestError("Fields 'name', 'rtype' and 'data' has to be specified", 400)
		if ttl != None:
			ttl = str(ttl)
		ctl.send_block(cmd="zone-set", zone=zone, owner=name, rtype=rtype, ttl=ttl, data=data)
		ctl.receive_block()
	except knot.KnotCtlErrorRemote as e:
		if e.message == 'no TTL specified':
			raise KnotRestError('New RRSET with no TTL specified', 400)
		raise e

# Add records in a zone.
#
# Parameters:
#	zone (mandatory): Zone name
#	name (optional): Record domain name
#	rtype (optional): Record type
#	data (optional): Percent encoded (URL encoded) record data
#	ttl (optional): Record set TTL
#
# Query Parameters:
#	name: Record domain name
#	ttl: Record set TTL
#	rtype: Record type
#	data: Percent encoded (URL encoded) record data
#
# Status codes:
# 	200 OK - An array of zones
# 	400 Bad Request - Missing or malformed data in a request
# 	409 Conflict - Record already exists
#	500 Internal Server Error - The server has encountered a situation it does not know how to handle
@bp.route('/zones/<zone>/records', methods=['PUT'])
@bp.route('/zones/<zone>/records/<name>', methods=['PUT'])
@bp.route('/zones/<zone>/records/<name>/<rtype>', methods=['PUT'])
@bp.route('/zones/<zone>/records/<name>/<rtype>/<data>', methods=['PUT'])
@bp.route('/zones/<zone>/records/<name>/<rtype>/<data>/<ttl>', methods=['PUT'])
@auth.login_required(role = ['zone'])
def records_put(zone, name = None, rtype = None, data = None, ttl = None):
	if zonelock_is_locked(zone):
		abort(503, 'Zone maintanance')

	# Get arguments
	if not name:
		name = get_argument('name')
	if not ttl:
		ttl = get_argument('ttl')
	if not rtype:
		rtype = get_argument('rtype')
	if not data:
		data = get_argument('data')

	# Use default TTL from config file
	if not ttl:
		ttl = current_app.config['default-ttl']

	# URL encoding
	if name:
		name = unquote(name)
	if data:
		data = unquote(data)

	if not name or not rtype or not data:
		abort(400, "Fields 'name', 'rtype' and 'data' has to be specified")

	# Get data
	try:
		with open_socket() as ctl:
			ctl.send_block(cmd="zone-read", zone=zone)
			old_zone_resp = ctl.receive_block()
			with zone_transaction(ctl, zone):
				_records_put(ctl, zone, name, rtype, ttl, data)
			ctl.send_block(cmd="zone-read", zone=zone)
			new_zone_resp = ctl.receive_block()
		old_zone = map_to_record_list(old_zone_resp)
		new_zone = map_to_record_list(new_zone_resp)
		removed, added = list_diff(old_zone, new_zone)
		if removed or added:
			getLogger("audit").info(f"User '{g.flask_httpauth_user.username}' add record(s) to zone '{zone}'\n{diff_to_string(removed, added)}")
		return jsonify(list(map(Record.serialize, added))), 201
	except knot.KnotCtlError as e:
		if e.message == 'parser failed':
			abort(400, 'Invalid parameter')
		if e.message == 'such record already exists in zone':
			abort(409, 'Record already exists')
		if e.message == 'no such zone found':
			abort(404, 'No such zone found')
		abort(500)
	except KnotRestError as e:
		e.abort()
	except HTTPException:
		raise
	except:
		abort(500)

def _records_patch(ctl, zone, name, rtype, ttl, data, new_name, new_rtype, new_ttl, new_data):
	try:
		if ttl != None:
			ttl = str(ttl)
		records = _records_get(ctl, zone, name, rtype, ttl, data)
		if len(records) == 0:
			raise KnotRestError("Record not found", 404)
		elif len(records) > 1:
			raise KnotRestError("Ambiguous filter", 404)
		r = records[0]
		_records_delete(ctl, zone, r.name, r.rtype, r.ttl, r.data, force = True)

		name = r.name if not new_name else new_name
		rtype = r.rtype if not new_rtype else new_rtype
		ttl = r.ttl if not new_ttl else new_ttl
		data = r.data if not new_data else new_data

		_records_put(ctl, zone, name, rtype, ttl, data)
	except:
		raise

# Change record in a zone.
#
# Parameters:
#	zone (mandatory): Zone name
#	name (mandatory): Record domain name
#	rtype (mandatory): Record type
#	data (mandatory): Percent encoded (URL encoded) record data
#
# Query Parameters:
#	name: Record domain name
#	ttl: Record set TTL
#	rtype: Record type
#	data: Percent encoded (URL encoded) record data
#
# Status codes:
# 	200 OK - An array of zones
# 	400 Bad Request - Missing or malformed data in a request
# 	404 Not Found - Record don't exist
#	500 Internal Server Error - The server has encountered a situation it does not know how to handle
@bp.route('/zones/<zone>/records/<name>/<rtype>/<data>', methods=['PATCH'])
@bp.route('/zones/<zone>/records/<name>/<rtype>/<data>/<ttl>', methods=['PATCH'])
@auth.login_required(role = ['zone'])
def records_patch(zone, name, rtype, data, ttl = None):
	if zonelock_is_locked(zone):
		abort(503, 'Zone maintanance')

	# Get arguments
	new_name = get_argument('name')
	new_ttl = get_argument('ttl')
	new_rtype = get_argument('rtype')
	new_data = get_argument('data')

	if not new_name and not new_ttl and not new_rtype and not new_data:
		abort(400, "No fields for change has been specified")

	# URL encoding
	name = unquote(name)
	data = unquote(data)
	if new_name:
		new_name = unquote(new_name)
	if new_data:
		new_data = unquote(new_data)

	# Get data
	try:
		with open_socket() as ctl:
			ctl.send_block(cmd="zone-read", zone=zone)
			old_zone_resp = ctl.receive_block()
			with zone_transaction(ctl, zone):
				_records_patch(ctl, zone, name, rtype, ttl, data, new_name, new_rtype, new_ttl, new_data)
			ctl.send_block(cmd="zone-read", zone=zone)
			new_zone_resp = ctl.receive_block()
		old_zone = map_to_record_list(old_zone_resp)
		new_zone = map_to_record_list(new_zone_resp)
		removed, added = list_diff(old_zone, new_zone)
		if removed or added:
			getLogger("audit").info(f"User '{g.flask_httpauth_user.username}' edited record in zone '{zone}'\n{diff_to_string(removed, added)}")
		return jsonify(list(map(Record.serialize, added)))
	except HTTPException:
		raise
	except knot.KnotCtlError as e:
		if e.message == 'parser failed':
			abort(400, 'Invalid parameter')
		if e.message == 'no such zone found':
			abort(404, 'No such zone found')
		abort(500)
	except KnotRestError as e:
		e.abort()
	except:
		abort(500)

def _records_delete(ctl, zone, name, rtype, ttl, data, force = False):
	try:
		if ttl != None:
			ttl = str(ttl)
		records = _records_get(ctl, zone, name, rtype, ttl, data)
		if (not force):
			records = list(filter(lambda el: el.rtype != 'SOA', records))
		if not records:
			raise KnotRestError("No record is matching to the filter", 404)
		for r in records:
			ctl.send_block(cmd="zone-unset", zone=r.zone, owner=r.name,
					rtype=r.rtype, data=r.data)
			ctl.receive_block()
	except:
		raise


# Delete records in a zone matching the filter. Return zone after update.
#
# Parameters:
#	zone (mandatory): Zone name
#	name (optional): Record domain name
#	rtype (optional): Record type
#	data (optional): Percent encoded (URL encoded) record data
#
# Query Parameters:
#	name: Record domain name
#	ttl: Record set TTL
#	rtype: Record type
#	data: Percent encoded (URL encoded) record data
#
# Status codes:
# 	200 OK - An array of zones
#	400 Bad Request - When try to delete SOA
# 	404 Not Found - Records don't match with filter
#	500 Internal Server Error - The server has encountered a situation it does not know how to handle
@bp.route('/zones/<zone>/records', methods=['DELETE'])
@bp.route('/zones/<zone>/records/<name>', methods=['DELETE'])
@bp.route('/zones/<zone>/records/<name>/<rtype>', methods=['DELETE'])
@bp.route('/zones/<zone>/records/<name>/<rtype>/<data>', methods=['DELETE'])
@bp.route('/zones/<zone>/records/<name>/<rtype>/<data>/<ttl>', methods=['DELETE'])
@auth.login_required(role = ['zone'])
def records_delete(zone, name = None, rtype = None, data = None, ttl = None):
	if zonelock_is_locked(zone):
		abort(503, 'Zone maintanance')

	# Get arguments
	if not name:
		name = get_argument('name')
	if not ttl:
		ttl = get_argument('ttl')
	if not rtype:
		rtype = get_argument('rtype')
	if not data:
		data = get_argument('data')
	
	# URL encoding
	if name:
		name = unquote(name)
	if data:
		data = unquote(data)

	if rtype == 'SOA':
		abort(400, "Unable to delete SOA records")

	# Get data
	try:
		with open_socket() as ctl:
			ctl.send_block(cmd="zone-read", zone=zone)
			old_zone_resp = ctl.receive_block()
			with zone_transaction(ctl, zone):
				_records_delete(ctl, zone, name, rtype, ttl, data)
			ctl.send_block(cmd="zone-read", zone=zone)
			new_zone_resp = ctl.receive_block()
		old_zone = map_to_record_list(old_zone_resp)
		new_zone = map_to_record_list(new_zone_resp)
		removed, added = list_diff(old_zone, new_zone)
		getLogger("audit").info(f"User '{g.flask_httpauth_user.username}' deleted record in zone '{zone}'\n{diff_to_string(removed, added)}")
		return jsonify(list(map(Record.serialize, added)))
	except HTTPException:
		raise
	except knot.KnotCtlError as e:
		if e.message == 'parser failed':
			abort(400, 'Invalid parameter')
		if e.message == 'no such zone found':
			abort(404, 'No such zone found')
		abort(500)
	except KnotRestError as e:
		e.abort()
	except:
		abort(500)


def proceed_request(ctl, zone, request):
	try:
		method = request.get('method', '').upper()
		if method == 'GET':
			fltr = request.get('filter', {})
			name = fltr.get('name')
			rtype = fltr.get('rtype')
			ttl = fltr.get('ttl')
			data = fltr.get('data')
			return _records_get(ctl, zone, name, rtype, ttl, data)
		elif method == 'PUT':
			input = request.get('input', {})
			new_name = input.get('name')
			new_rtype = input.get('rtype')
			new_ttl = input.get('ttl')
			new_data = input.get('data')
			# Use default TTL from config file
			if not new_ttl:
				new_ttl = current_app.config['default-ttl']
			_records_put(ctl, zone, new_name, new_rtype, new_ttl, new_data)
			return ()
		elif method == 'PATCH':
			fltr = request.get('filter', {})
			name = fltr.get('name')
			rtype = fltr.get('rtype')
			ttl = fltr.get('ttl')
			data = fltr.get('data')
			input = request.get('input', {})
			new_name = input.get('name')
			new_rtype = input.get('rtype')
			new_ttl = input.get('ttl')
			new_data = input.get('data')
			_records_patch(ctl, zone, name, rtype, ttl, data, new_name, new_rtype, new_ttl, new_data)
			return ()
		elif method == 'DELETE':
			fltr = request.get('filter', {})
			name = fltr.get('name')
			rtype = fltr.get('rtype')
			ttl = fltr.get('ttl')
			data = fltr.get('data')
			_records_delete(ctl, zone, name, rtype, ttl, data)
			return ()
		else:
			raise KnotRestOperationError(request, f"Unknown method '{method}'")
	except KnotRestError as e:
		e.abort()

@bp.route('/zones/<zone>/records', methods=['POST'])
@auth.login_required(role = ['zone'])
def records_post(zone):
	if zonelock_is_locked(zone):
		abort(503, 'Zone maintanance')

	if not request.is_json:
		abort(400, 'Not JSON request')
	operations = request.get_json()
	if type(operations) is not list:
		abort(400, 'JSON request must be list')

	try:
		output = set()
		with open_socket() as ctl:
			ctl.send_block(cmd="zone-read", zone=zone)
			old_zone_resp = ctl.receive_block()
			with zone_transaction(ctl, zone):
				for r in operations:
					output.update(set(proceed_request(ctl, zone, r)))
			ctl.send_block(cmd="zone-read", zone=zone)
			new_zone_resp = ctl.receive_block()
		old_zone = map_to_record_list(old_zone_resp)
		new_zone = map_to_record_list(new_zone_resp)
		removed, added = list_diff(old_zone, new_zone)
		if removed or added:
			getLogger("audit").info(f"User '{g.flask_httpauth_user.username}' changed records in zone '{zone}' using post method\n{diff_to_string(removed, added)}")
		return jsonify(list(map(Record.serialize, output)))
	except HTTPException:
		raise
	except knot.KnotCtlErrorRemote as e:
		if e.message == 'no such zone found':
			abort(404, 'No such zone found')
		abort(500)
	except KnotRestError as e:
		e.abort()
	except Exception as e:
		abort(500)