from flask import Flask

_ERRORS = {
	400: "Bad request",
	401: "Unauthorized",
	404: "Not found",
	409: "Conflict",
	500: "Internal server error",
	503: "Service unavailable"
}

def register_error_handlers(self : Flask):
	for code in _ERRORS:
		self.register_error_handler(code, lambda e: ({
				"Code": e.code,
				"Error": _ERRORS[e.code],
				"Description": e.description
			}, e.code))