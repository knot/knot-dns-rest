from flask import abort

class KnotRestError(Exception):
	def __init__(self, message, code = 500):
		self.strerror = message
		self.code = code

	def abort(self):
		raise abort(self.code, self.strerror)

class KnotRestConfigError(KnotRestError):
	def __init__(self, message):
		KnotRestError.__init__(self, f"Configuration error: {message}", 400)

class KnotRestUserError(KnotRestError):
	def __init__(self, uname, message = "User error"):
		KnotRestError.__init__(self, message, 400)
		self.username = uname

class KnotRestOperationError(KnotRestError):
	def __init__(self, request, message = "Operation error"):
		KnotRestError.__init__(self, message, 404)
		self.request = request