from click import echo
from flask import Blueprint
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, migrate, upgrade
from os.path import dirname
from sys import exit

from . import migrations

db = SQLAlchemy()
migration = Migrate(db=db)

bp = Blueprint('database', __name__)
bp.cli.short_help = "Database manipulation."
bp.cli.help = "This group of commands allows standardized manipulation of the database."

@bp.cli.command("init", help="Initialize database and tables.")
def init_db():
	try:
		db.create_all()
		echo("Success")
	except:
		echo("Unable to create database tables")
		exit(1)

@bp.cli.command('upgrade', help="Upgrade database schema.")
def upgrade_db():
	try:
		upgrade(directory=dirname(migrations.__file__))
		echo("Success")
	except Exception  as e:
		echo("Unable to upgrade database schema")
		exit(1)

@bp.cli.command('migrate', help="Creates new update schema for db (devs-only).")
def migrate_db():
	try:
		migrate(directory=dirname(migrations.__file__))
		echo("Success")
	except Exception  as e:
		echo("Unable to upgrade database schema")
		exit(1)