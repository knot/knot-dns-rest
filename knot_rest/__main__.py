#!/usr/bin/env python3

from argparse import ArgumentParser, Namespace
from sys import exit
from waitress import serve

from knot_rest import create_app

parser = ArgumentParser()
parser.add_argument('-c', '--config', nargs='?', type=str, help='specify path to config file')
parser.add_argument('-l', '--listen', nargs='*', action='extend', type=str, help='address to which the server will listen for queries')
parser.add_argument('-S', '--https', default='http', action='store_const', const='https', help='use this flag when queries are proxied from HTTPS server')

def parse_args() -> Namespace:
	args = parser.parse_args()

	# Currently I haven't found a way to set a default value, and at the same
	# time allow to set several different parameters in the program arguments.
	if not args.listen:
		args.listen = ["127.0.0.1:8080"]

	return args

def main() -> int:
	args = parse_args()
	app = create_app(config=args.config)
	if not app:
		return 1

	# Start WSGI server
	app.logger.info('Server is running ...')
	serve(app, listen=args.listen, url_scheme=args.https)

	return 0

if __name__ == '__main__':
	exit(main())