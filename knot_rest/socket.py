from contextlib import contextmanager
from flask import current_app
from libknot.control import KnotCtl

@contextmanager
def open_socket():
	try:
		ctl = KnotCtl()
		ctl.connect(current_app.config['socket']['path'])
		if current_app.config['socket']['timeout']:
			ctl.set_timeout(current_app.config['socket']['timeout'])
		yield ctl
	finally:
		ctl.close()

@contextmanager
def zone_transaction(ctl : KnotCtl, zone):
	try:
		ctl.send_block(cmd="zone-begin", zone=zone)
		ctl.receive_block()
		yield
	except:
		ctl.send_block(cmd="zone-abort")
		ctl.receive_block()
		raise
	else:
		ctl.send_block(cmd="zone-commit")
		ctl.receive_block()