#!/usr/bin/env bash

cd `git rev-parse --show-toplevel` \
	&& python3 -m venv .venv \
	&& . .venv/bin/activate \
	&& pip install -r requirements.txt \
	&& pip install -e .