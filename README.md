# Knot-DNS-REST

## About

`Knot DNS REST` is a Python application that mediates communication between a
[Knot DNS](https://gitlab.nic.cz/knot/knot-dns) server and the HTTP REST API.

## Installation

### Pip package

Not published yet

`knot_rest` in the future

### Git source

Download from git

Run `./dev-ops/bootstrap.sh` for setup virtual environment and install
packages. Activate virtual environment (`source ./.venv/bin/activate`) to load
all dependencies.

## Preparation

### Configuration

The configuration file is in YAML format. The default file search locations are
`/etc/knot_rest/knot_rest.yaml` and `./knot_rest.yaml`, or can be
specified as argument (run the package as
`knot_rest:create_app("/path/to/config.yaml")`, see [Run](#run)).

**Example:**

```
audit-log: '/var/log/knot_rest/audit.log'

database: 'sqlite:////var/lib/knot_rest/database.db'

libknot: '/usr/local/lib/libknot.so'

socket:
  path: '/var/run/knot.sock'
  timeout: 60

token:
  secret: '32Ly9nNXtuorOK3OoaL2riMTbR7LLJpX'
  expiration: 600
```

**Explanation:**

 * **audit-log** - Path to file that will be used for audit logging. Log files are rotated every midnight.
 * **database** - Path to the database file
 * **libknot** - Path to the shared libknot object distributed with Knot DNS
 * **socket.path** - Path to the Unix socket created by the Knot DNS daemon
 * **socket.timeout** - Configure the timeout of the Unix socket (time to receive a response from the socket)
 * **token.secret** - The string used to sign the JWT token (the token used for authentication) . If set, tokens remain valid between reboots. If omitted, it will be generated at startup.
 * **token.expiration** - The expiration time of the token.

### Run Knot DNS

Knot REST needs a connection to the Knot DNS socket. Run Knot DNS and set the
correct path to the socket in the configuration to match between applications.

### Database

To initialize database layout you can call command `flask database init`.

Remember to specify the `flask --app <path> ...` argument if you are using
non-standard settings, see [Run](#flask-wsgi-development))

### Version upgrade

When upgrading Knot DNS REST, some changes might appear in database schema.
Upgrade of database schema by using `flask database upgrade` command.

### Add admin user

To work with REST API, you will need user credentials. To create new user you
can call command `flask users add <username> <password>`. You will also need to
make user admin by calling `flask usermod admin add <username>`.

More info about CLI commands you will find [here](#cli-commands).

## Run

Recommended deployment is using a standalone WSGI server listening on HTTPS
only. Do not run WSGI server as `root`.

### Standalone WSGI

There are plenty of WSGI servers available to run, which you can select as
needed. As an example, here is the `gunicorn` server:

```
pip install gunicorn
gunicorn 'knot_rest' --bind 127.0.0.1:8080 --keyfile keyfile.key --certfile certfile.cert -w `nproc`
```

You can also specify configuration file path:

```
pip install gunicorn
gunicorn 'knot_rest:create_app("/path/to/config.yaml")' --bind 127.0.0.1:8080 --keyfile keyfile.key --certfile certfile.cert -w `nproc`
```

For more information visit `gunicorn` documentation.

### Buildin WSGI (minimalistic)

There is `Waitress` WSGI build in the application. There is not support for
HTTPS, for HTTPS use reverse proxy. You can start server with:

```
python3 -m knot_rest -l 127.0.0.1:5000 -s /path/to/config.yaml
```

### Flask WSGI (development)

```
flask run
```

You can also specify config file similar way as with `gunicorn`.

```
flask --app 'knot_rest:create_app("/path/to/config.yaml")' run
```
## CLI commands

Some actions could be done using CLI commands from `flask`:

| command |          |             |              |              |                 | description                                                                   |
|---------|----------|-------------|--------------|--------------|-----------------|-------------------------------------------------------------------------------|
| flask   | --help   |             |              |              |                 | Prints help (works also with subcommands)                                     |
|         | database | init        |              |              |                 | Initialize database tables                                                    |
|         | users    | add         | `<username>` | `<password>` | `<description>` | Create new user                                                               |
|         |          | remove      | `<username>` |              |                 | Remove user                                                                   |
|         |          | reset       | `<username>` | `<password>` |                 | Set new password for user                                                     |
|         |          | list        |              |              |                 | List users                                                                    |
|         | usermod  | admin       | add          | `<username>` |                 | Add user administrator rights                                                 |
|         |          |             | del          | `<username>` |                 | Remove users administrator rights                                             |
|         |          | zone        | add          | `<username>` | `<zonename>`    | Add user rights to work with specific zone                                    |
|         |          |             | del          | `<username>` | `<zonename>`    | Remove rights to work with specific zone of user                              |
|         |          | description | set          | `<username>` | `<description>` | Set user description                                                          |
|         | service  | disable     | [`-f`]       | `<zonename>` |                 | Disable record changes of specified zone ( `-f` for disable nonexistent zone) |
|         |          | enable      |              | `<zonename>` |                 | Enable record changes of specified zone                                       |
|         |          | status      | [`-v`]       |              |                 | Print status of service ( `-v` for more detailed information)                 |

## REST API

For call of remote process you should use custom client. But for testing, you can use `curl` command for example:

```
curl -X PUT -d "name=dns1.example.com." -d "type=AAAA" -d "ttl=3600" -d "data=123.45.67.89" 127.0.0.1:8080/zones/example.com./records  | jq
# NOTE: This is just an example, this might delete record from you zone. For pretty JSON output use 'jq'
curl -X DELETE -d "name=dns1.example.com." -d "type=AAAA" 127.0.0.1:8080/zones/example.com./records  | jq
```

### Login/Registration

To obtain authorization token, send following request where `-u` are users credentials in format `<username>:<password>`.

```
curl -u dev:dev 127.0.0.1:5000/user/login
```

For sending request as logged in user, add authorization token in packet header like this:

```
curl -H "Authorization: Bearer <token>" ...
```

Only an existing user can register users. The token of the logged-in user is
required, as well as the username and password of the new user.

```
curl -X POST -H "Authorization: Bearer <token>" -d "username=<username>" -d "password=<password> 127.0.0.1:5000/user/register
```

### Commands

In the tables below, parameters labeled `path` are specified in URL path as `/zones/example.com/records/dns1.example.com/A`. Parameters labeled `query` are specified in URL query as `/zones/example.com/records?name=dns1.example.com&rtype=A`. Parameters labeled `HTTP POST data` are stored in HTTP header (the method of transmission depends on the client).

***

`GET /user/info/<uname>`

List name and access rights of specified user

**Parameters:**

| name  | description | mandatory | path | query / HTTP POST data |
|-------|-------------|:---------:|:----:|:----------------------:|
| uname | User name   |           |   x  |            x           |

**Status codes:**
* *200 OK* - Successful
* *403 Forbidden* - Not allowed
* *404 Not Found* - Name not found
* *500 Internal Server Error* - The server has encountered a situation it does not know how to handle

***

`GET /zones/<zone>`

List all (or specified) zones on a server.

**Parameters:**

| name | description | mandatory | path | query / HTTP POST data |
|------|-------------|:---------:|:----:|:----------------------:|
| zone | Zone name   |           |   x  |            x           |

**Status codes:**
* *200 OK* - Successful
* *404 Not Found* - Requested item was not found
* *500 Internal Server Error* - The server has encountered a situation it does not know how to handle

***

`GET /zones/<zone>/records/<name>/<rtype>/<data>/<ttl>`

List all records in a zone that match parameters.

**Parameters:**

| name  | description                      | mandatory | path | query / HTTP POST data |
|-------|----------------------------------|:---------:|:----:|:----------------------:|
| zone  | Zone name                        |     x     |   x  |                        |
| name  | Record domain name (URL encoded) |           |   x  |            x           |
| rtype | Record type                      |           |   x  |            x           |
| data  | Record data (URL encoded)        |           |   x  |            x           |
| ttl   | Record TTL                       |           |   x  |            x           |

**Status codes:**
* *200 OK* - Successful
* *404 Not Found* - Requested item was not found
* *500 Internal Server Error* - The server has encountered a situation it does not know how to handle

***

`PUT /zones/<zone>/records/<name>/<rtype>/<data>/<ttl>`

Add record in a zone. Return zone after changes.

**Parameters:**

| name  | description                      | mandatory | path | query / HTTP POST data |
|-------|----------------------------------|:---------:|:----:|:----------------------:|
| zone  | Zone name                        |     x     |   x  |                        |
| name  | Record domain name (URL encoded) |           |   x  |            x           |
| rtype | Record type                      |           |   x  |            x           |
| data  | Record data (URL encoded)        |           |   x  |            x           |
| ttl   | Record TTL                       |           |   x  |            x           |

**Status codes:**
* *201 CREATED* - Successful
* *400 Bad Request* - Missing or malformed data in a request
* *409 Conflict* - Record already exists
* *500 Internal Server Error* - The server has encountered a situation it does not know how to handle

***

`PATCH /zones/<zone>/records/<name>/<rtype>/<data>`

Change record in a zone. Returns changed record.

**Parameters:**

| name  | description                          | mandatory | path | query / HTTP POST data |
|-------|--------------------------------------|:---------:|:----:|:----------------------:|
| zone  | Zone name                            |     x     |   x  |                        |
| name  | Record domain name (URL encoded)     |     x     |   x  |                        |
| rtype | Record type                          |     x     |   x  |                        |
| data  | Record data (URL encoded)            |     x     |   x  |                        |
| name  | New record domain name (URL encoded) |           |      |            x           |
| rtype | New record type                      |           |      |            x           |
| data  | New record data (URL encoded)        |           |      |            x           |
| ttl   | New record TTL                       |           |      |            x           |

**Status codes:**
* *200 OK* - Successful
* *400 Bad Request* - Missing or malformed data in a request
* *404 Not Found* - Record don't exist
* *500 Internal Server Error* - The server has encountered a situation it does not know how to handle

***

`DELETE /zones/<zone>/records/<name>/<rtype>/<data>/<ttl>`

Delete records in a zone matching the filter. Return zone after update.

**Parameters:**

| name  | description                      | mandatory | path | query / HTTP POST data |
|-------|----------------------------------|:---------:|:----:|:----------------------:|
| zone  | Zone name                        |     x     |   x  |                        |
| name  | Record domain name (URL encoded) |           |   x  |            x           |
| rtype | Record type                      |           |   x  |            x           |
| data  | Record data (URL encoded)        |           |   x  |            x           |
| ttl   | Record TTL                       |           |   x  |            x           |

**Status codes:**
* *200 OK* - An array of records
* *400 Bad Request* - Trying to delete SOA
* *404 Not Found* - Requested item was not found
* *500 Internal Server Error* - The server has encountered a situation it does not know how to handle


`POST /zones/<zone>/records`

Manipulate multiple records in a zone based on list of requests posted as JSON.
List of requests is list of object *operation* (for more info visit section
Objects). Returns list of records that match filter for GET requests.

**Parameters:**

| name  | description                      | mandatory | path | query / HTTP POST data |
|-------|----------------------------------|:---------:|:----:|:----------------------:|
| zone  | Zone name                        |     x     |   x  |                        |

**Status codes:**
* *200 OK* - An array of records
* *400 Bad Request* - Missing or malformed data in a request, or tying to delete SOA
* *404 Not Found* - Requested item was not found
* *409 Conflict* - Record already exists
* *500 Internal Server Error* - The server has encountered a situation it does not know how to handle

`GET /control/zone/status/<zone>`

Show the zone status. Filters may be used to show/silence some outputs (might be
combined). Only `admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| filters | Output filters                   |           |      |            x           |

**Filters**
* *c* - catalog
* *e* - events
* *f* - freeze
* *r* - role
* *s* - serial
* *t* - transaction

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/status`

Show the zone status. Can be used to specify multiple zones at once. Filters may
be used to show/silence some outputs (might be combined). Use
*Control operation over multiple zones* object as data. Only `admin` can use
control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| filters | Output filters                   |           |      |            x           |

**Filters**
* *c* - catalog
* *e* - events
* *f* - freeze
* *r* - role
* *s* - serial
* *t* - transaction

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/reload/<zone>`

Trigger a zone reload from a disk without checking its modification time.
For secondary zone, the refresh event from primary server(s) is scheduled,
for primary zone, the notify event to secondary server(s) is scheduled. An open
zone transaction will be aborted! Only `admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking
* *F* - force

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/reload`

Trigger a zone reload from a disk without checking its modification time.
For secondary zone, the refresh event from primary server(s) is scheduled,
for primary zone, the notify event to secondary server(s) is scheduled. An open
zone transaction will be aborted! Use *Control operation over multiple zones*
object as data. Only `admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking
* *F* - force

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/refresh/<zone>`

Trigger a check for the zone serial on the zone's primary server. If the primary
server has a newer zone, a transfer is scheduled. This command is valid for
secondary zones. Only `admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/refresh`

Trigger a check for the zone serial on the zone's primary server. If the primary
server has a newer zone, a transfer is scheduled. This command is valid for
secondary zones. Use *Control operation over multiple zones* object as data.
Only `admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/retransfer/<zone>`

Trigger a zone transfer from the zone's primary server. The server doesn't check
the serial of the primary server's zone. This command is valid for secondary
zones. Only `admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/retransfer`

Trigger a zone transfer from the zone's primary server. The server doesn't check
the serial of the primary server's zone. This command is valid for secondary
zones. Use *Control operation over multiple zones* object as data. Only `admin`
can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/notify/<zone>`

Trigger a NOTIFY message to all configured remotes. This can help in cases when
previous NOTIFY had been lost or the secondary servers have been offline. Only
`admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/notify`

Trigger a NOTIFY message to all configured remotes. This can help in cases when
previous NOTIFY had been lost or the secondary servers have been offline. Use
*Control operation over multiple zones* object as data. Only `admin` can use
control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/flush/<zone>`

Trigger a zone journal flush to the configured zone file. If an output directory
is specified, the current zone is immediately dumped to a zone file in the
specified directory. Only `admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| dir     | Output directory                 |     x     |      |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/flush`

Trigger a zone journal flush to the configured zone file. If an output directory
is specified, the current zone is immediately dumped to a zone file in the
specified directory. Use *Control operation over multiple zones* object as data.
Only `admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| dir     | Output directory                 |     x     |      |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/backup/<zone>`

Trigger a zone data and metadata backup to a specified directory. With available
filters set, zone contents, zone's journal, zone-related timers, zone-related
data in the KASP database together with keys (or keys without the KASP
database), zone's catalog, and the server QUIC key and certificate are backed
up, or omitted from the backup. By default, filters +zonefile, +timers,
+kaspdb, +catalog, +quic, +nojournal, and +nokeysonly are set for backup.
Setting a filter for an item doesn't change the default settings for other
items. The only exception is +keysonly, which disables all other filters by
default, but they can still be turned on explicitly. If zone flushing is
disabled, the original zone file is backed up instead of writing out zone
contents to a file. When backing-up a catalog zone, it is recommended to prevent
ongoing changes to it by use of zone-freeze. Only `admin` can use control
commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| filters | Filters                          |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |
| dir     | Output directory                 |     x     |      |            x           |

**Filters**
* *c*/*C* - catalog/nocatalog
* *j*/*J* - journal/nojournal
* *k*/*K* - kaspdb/nokaspdb
* *o*/*O* - keysonly/nokeysonly
* *t*/*T* - timers/notimers
* *q*/*Q* - quic/noquic
* *z*/*Z* - zonefile/nozonefile

**Flags**
* *B* - blocking
* *F* - force

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/backup`

Trigger a zone data and metadata backup to a specified directory. With available
filters set, zone contents, zone's journal, zone-related timers, zone-related
data in the KASP database together with keys (or keys without the KASP
database), zone's catalog, and the server QUIC key and certificate are backed
up, or omitted from the backup. By default, filters +zonefile, +timers,
+kaspdb, +catalog, +quic, +nojournal, and +nokeysonly are set for backup.
Setting a filter for an item doesn't change the default settings for other
items. The only exception is +keysonly, which disables all other filters by
default, but they can still be turned on explicitly. If zone flushing is
disabled, the original zone file is backed up instead of writing out zone
contents to a file. When backing-up a catalog zone, it is recommended to prevent
ongoing changes to it by use of zone-freeze. Use
*Control operation over multiple zones* object as data. Only `admin` can use
control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| filters | Filters                          |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |
| dir     | Output directory                 |     x     |      |            x           |

**Filters**
* *c*/*C* - catalog/nocatalog
* *j*/*J* - journal/nojournal
* *k*/*K* - kaspdb/nokaspdb
* *o*/*O* - keysonly/nokeysonly
* *t*/*T* - timers/notimers
* *q*/*Q* - quic/noquic
* *z*/*Z* - zonefile/nozonefile

**Flags**
* *B* - blocking
* *F* - force

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/restore/<zone>`

Trigger a zone data and metadata restore from a specified backup directory.
Optional filters are equivalent to the same filters of zone-backup. Only `admin`
can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| filters | Filters                          |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |
| dir     | Output directory                 |     x     |      |            x           |

**Filters**
* *c*/*C* - catalog/nocatalog
* *j*/*J* - journal/nojournal
* *k*/*K* - kaspdb/nokaspdb
* *o*/*O* - keysonly/nokeysonly
* *t*/*T* - timers/notimers
* *q*/*Q* - quic/noquic
* *z*/*Z* - zonefile/nozonefile

**Flags**
* *B* - blocking
* *F* - force

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/restore`

Trigger a zone data and metadata restore from a specified backup directory.
Optional filters are equivalent to the same filters of zone-backup. Use
*Control operation over multiple zones* object as data. Only `admin` can use
control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| filters | Filters                          |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |
| dir     | Output directory                 |     x     |      |            x           |

**Filters**
* *c*/*C* - catalog/nocatalog
* *j*/*J* - journal/nojournal
* *k*/*K* - kaspdb/nokaspdb
* *o*/*O* - keysonly/nokeysonly
* *t*/*T* - timers/notimers
* *q*/*Q* - quic/noquic
* *z*/*Z* - zonefile/nozonefile

**Flags**
* *B* - blocking
* *F* - force

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/sign/<zone>`

Trigger a DNSSEC re-sign of the zone. Existing signatures will be dropped. This
command is valid for zones with DNSSEC signing enabled. Only `admin` can use
control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/sign`

Trigger a DNSSEC re-sign of the zone. Existing signatures will be dropped. This
command is valid for zones with DNSSEC signing enabled. Use
*Control operation over multiple zones* object as data. Only `admin` can use
control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/validate/<zone>`

Trigger a DNSSEC validation of the zone. If the validation fails and the zone is
secondary, the zone expires immediately! Only `admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/validate`

Trigger a DNSSEC validation of the zone. If the validation fails and the zone is
secondary, the zone expires immediately! Use
*Control operation over multiple zones* object as data. Only `admin` can use
control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/keys-load/<zone>`

Trigger a load of DNSSEC keys and other signing material from KASP database
(which might have been altered manually). If suitable, re-sign the zone
afterwards (keeping valid signatures intact). Only `admin` can use control
commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/keys-load`

Trigger a load of DNSSEC keys and other signing material from KASP database
(which might have been altered manually). If suitable, re-sign the zone
afterwards (keeping valid signatures intact). Use
*Control operation over multiple zones* object as data. Only `admin` can use
control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/key-rollover/<zone>`

Trigger immediate key rollover. Publish new key and start a key rollover, even
when the key has a lifetime to go. Key type can be ksk (also for CSK) or zsk.
This command is valid for zones with DNSSEC signing and automatic key management
enabled. Note that complete key rollover consists of several steps and the
blocking mode relates to the initial one only! Only `admin` can use control
commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |     x     |   x  |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/ksk-submitted/<zone>`

Use when the zone's KSK rollover is in submission phase. By calling this command
the user confirms manually that the parent zone contains DS record for the new
KSK in submission phase and the old KSK can be retired. Only `admin` can use
control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |     x     |   x  |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/ksk-submitted`

Use when the zone's KSK rollover is in submission phase. By calling this command
the user confirms manually that the parent zone contains DS record for the new
KSK in submission phase and the old KSK can be retired. Use
*Control operation over multiple zones* object as data. Only `admin` can use
control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |     x     |      |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/freeze/<zone>`

Trigger a zone freeze. All running events will be finished and all new and
pending (planned) zone-changing events (load, refresh, update, flush, and DNSSEC
signing) will be held up until the zone is thawed. Up to 8 (this limit is
hardcoded) DDNS updates per zone will be queued, subsequent updates will be
refused. Only `admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/freeze`

Trigger a zone freeze. All running events will be finished and all new and
pending (planned) zone-changing events (load, refresh, update, flush, and DNSSEC
signing) will be held up until the zone is thawed. Up to 8 (this limit is
hardcoded) DDNS updates per zone will be queued, subsequent updates will be
refused. Use *Control operation over multiple zones* object as data. Only
`admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/thaw/<zone>`

Trigger dismissal of zone freeze. Only `admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/thaw`

Trigger dismissal of zone freeze. Use *Control operation over multiple zones*
object as data. Only `admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/xfr-freeze/<zone>`

Temporarily disable outgoing AXFR/IXFR for the zone(s). Only `admin` can use
control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/xfr-freeze`

Temporarily disable outgoing AXFR/IXFR for the zone(s). Use
*Control operation over multiple zones* object as data. Only `admin` can use
control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`GET /control/zone/xfr-thaw/<zone>`

Dismiss outgoing XFR freeze. Only `admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |   x  |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

`POST /control/zone/xfr-thaw`

Dismiss outgoing XFR freeze. Use *Control operation over multiple zones* object
as data. Only `admin` can use control commands.

**Parameters:**

| name    | description                      | mandatory | path | query / HTTP POST data |
|---------|----------------------------------|:---------:|:----:|:----------------------:|
| zone    | Zone name                        |           |      |            x           |
| flags   | Command flags                    |           |      |            x           |

**Flags**
* *B* - blocking

**Status codes:**
* *200 OK* - An dictionary of zone status
* *400 Bad Request* - Unsupported operation, flag or filter
* *404 Not Found* - Zone not found

### Objects

**Error**

```
{
  "Code": 404,
  "Description": "Zone not found",
  "Error": "Not found"
}
```

* Description - Description of the source of the error
* Error - HTTP error code name

**Zone**

```
{
  "name": example.com.,
  "url": "/zones/example.com.,
  "serial": 1,
  "records": [
    {
      "data": "dns1.example.com. hostmaster.example.com. 1 21600 3600 604800 86400",
      "name": "example.com.",
      "rtype": "SOA",
      "ttl": "3600",
      "url": "/zones/example.com./records/example.com./SOA/dns1.example.com.%20hostmaster.example.com.%201%2021600%203600%20604800%2086400"
    }
  ]   
}
```

* name - Zone name
* url - Path that uniquely identifies the zone object
* serial - Serial number of the zone
* records - Array of the records relevant to the zone

**Record**

```
{
  "data": "dns1.example.com. hostmaster.example.com. 2010111227 21600 3600 604800 86400",
  "name": "example.com.",
  "rtype": "SOA",
  "ttl": "3600",
  "url": "/zones/example.com./records/example.com./SOA/dns1.example.com.%20hostmaster.example.com.%202010111227%2021600%203600%20604800%2086400"
}
```

* data - Record data
* name - Record name
* rtype - Record type
* ttl - Record time to live
* url - Path that uniquely identifies the record object

**Operation**

```
{
  "method": "PATCH",
  "filter": {
    "name": "example.com.",
    "rtype": "A",
    "ttl": "3600",
    "data": "1.2.3.4"
  },
  "input": {
    "name": "dns1.example.com.",
    "rtype": "A",
    "ttl": "3600",
    "data": "2.3.4.5"
  }
}
```

* method - Method to be performed (GET, PUT, DELETE, PATCH)
* filter - Original record description for GET, DELETE and PATCH methods (detail of filter dict is same as **Record** object)
* input - New record description for PUT and PATCH methods (detail of input dict is same as **Record** object)

**Control operation over multiple zones**

```
{
  "filters": "abcd",
  "flags": "ABCD",
  "dir": "/path/to/dir",
  "zones": [
    "example.com",
    "example.net"
  ]
}
```

* filters - Value is specified by some operations. Can filter the output. Can be
omitted.
* flags - Value is specified by some operations. Can be used to change the
behavior of commands.
* dir - Some operations require specified output directory. Can be omitted by
rest of operations.
* zones - List of zones on which the operation should be performed.