from random import choice, randrange
from string import ascii_letters, digits
from urllib.parse import quote

def test_init_db(runner):
	assert runner.invoke(args=['database', 'init']).exit_code == 0
	pass

def test_users_add(runner):
	assert runner.invoke(
			args=['users', 'add', 'test', 'test']
		).exit_code == 1

	username = ''.join(choice(ascii_letters) for _ in range(8))
	assert runner.invoke(
			args=['users', 'add', username, 'password']
		).exit_code == 0

def test_usermod_zone(runner):
	# Add zone rights
	assert runner.invoke(
			args=['usermod', 'zone', 'add', 'test', 'test.']
		).exit_code == 0

	# Try again
	assert runner.invoke(
			args=['usermod', 'zone', 'add', 'test', 'test.']
		).exit_code == 0
	
	# Add zone right for unexistent user
	username = ''.join(choice(ascii_letters) for _ in range(8))
	assert runner.invoke(
			args=['usermod', 'zone', 'add', username, 'test.']
		).exit_code == 1
	
	assert runner.invoke(
			args=['usermod', 'zone', 'del', 'test', 'test.']
		).exit_code == 0
	
	assert runner.invoke(
			args=['usermod', 'zone', 'del', username, 'test.']
		).exit_code == 1


def test_usermod_admin(runner):
	assert runner.invoke(
			args=['usermod', 'admin', 'add', 'test']
		).exit_code == 0

	username = ''.join(choice(ascii_letters) for _ in range(8))
	assert runner.invoke(
			args=['usermod', 'admin', 'add', username]
		).exit_code == 1

	assert runner.invoke(
			args=['usermod', 'admin', 'del', 'test']
		).exit_code == 0

	assert runner.invoke(
			args=['usermod', 'admin', 'del', username]
		).exit_code == 1

def test_usermod_description(runner):
	assert runner.invoke(
			args=['usermod', 'description', 'set', 'test', 'description']
		).exit_code == 0

def _random_owner():
	return ''.join(choice(ascii_letters + digits) for _ in range(20))

def _random_ip4():
	return '.'.join(str(randrange(256)) for _ in range(4))

def test_service(runner, auth):
	auth.login('test_admin')
	zonename = 'test.'

	owner = _random_owner()
	ip = _random_ip4()
	ttl = randrange(65536)
	# Put zone of specific name in path
	response = auth.put(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}/{ttl}')
	assert response.status_code == 201

	# Disable zone services
	assert runner.invoke(
			args=['service', 'disable', zonename]
		).exit_code == 0

	owner = _random_owner()
	ip = _random_ip4()
	ttl = randrange(65536)
	# Put another zone should be impossible
	response = auth.put(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}/{ttl}')
	assert response.status_code == 503

	# Try disable already disabled zone services
	assert runner.invoke(
			args=['service', 'disable', zonename]
		).exit_code == 1

	# Enable zone services
	assert runner.invoke(
			args=['service', 'enable', zonename]
		).exit_code == 0

	# Try enable already enable zone services
	assert runner.invoke(
			args=['service', 'enable', zonename]
		).exit_code == 1

	# Put zone should be possible again
	response = auth.put(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}/{ttl}')
	assert response.status_code == 201
