from libknot import Knot as LibKnot, control as knot
from os import getenv, mkdir
from os.path import exists, join
from random import randint
from shutil import copy
from signal import SIGINT
from subprocess import Popen, TimeoutExpired
from tempfile import mkdtemp

from knot_rest.exceptions import KnotRestError

def _timed_wait(process : Popen, timeout):
	try:
		process.wait(timeout)
	except TimeoutExpired:
		pass
	return process.returncode

class Knot:
	def __init__(self):
		self.storage = mkdtemp(prefix='knot-dns-rest-test')
		pass

	@property
	def conf_path(self):
		return join(self.storage, 'knot.conf')

	@property
	def log_path(self):
		return join(self.storage, 'knot.log')

	@property
	def zonefiles_path(self):
		return join(self.storage, 'zones/')

	@property
	def socket_path(self):
		return join(self.storage, 'knot.sock')

	def _copy_zonefile(self):
		mkdir(self.zonefiles_path)
		copy('./tests/knot/test.zone', join(self.zonefiles_path, 'test.zone'))

	def _create_conf(self):
		port = randint(49152, 65535)
		with open(self.conf_path, 'w') as config_file:
			config_file.write( 'server:\n')
			config_file.write(f'    listen: [0.0.0.0@{port}, ::@{port}]\n')
			config_file.write(f'    rundir: {self.storage}\n')
			config_file.write( 'zone:\n')
			config_file.write( '  - domain: test\n')
			config_file.write( '    file: "%s.zone"\n')
			config_file.write(f'    storage: {self.zonefiles_path}\n')
			config_file.write( '  - domain: test2\n')
			config_file.write( '    file: "%s.zone"\n')
			config_file.write(f'    storage: {self.zonefiles_path}\n')
			config_file.write( 'log:\n')
			config_file.write(f'  - target: {self.log_path}\n')
			config_file.write(f'    any: info\n')
			config_file.write(f'database:\n')
			config_file.write(f'    storage: {self.storage}\n')

	def start(self):
		self._copy_zonefile()
		self._create_conf()

		knotd = getenv('KNOTD_EXEC')
		self._process = Popen([knotd, '-c', self.conf_path])

		# Wait for Knot DNS socket
		while True:
			if exists(self.socket_path):
				break

			if self._process.returncode:
				raise KnotRestError('Knot DNS did not start')
			_timed_wait(self._process, 1)

		# Wait for Knot DNS ready
		libknot = getenv('KNOT_LIB')
		try:
			LibKnot(libknot)
			ctl = knot.KnotCtl()
			ctl.connect(self.socket_path)
			ctl.send_block(cmd="status")
			ctl.receive_block()
			ctl.close()
		except:
			raise KnotRestError('Knot DNS did not start')

	def stop(self):
		self._process.send_signal(SIGINT)
		self._process.wait()