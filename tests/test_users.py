def test_register(auth):
	# Test working registration
	auth.login('test_admin')
	response = auth.post('/user/register', data={'username': 'registered', 'password': 'registered'})
	assert response.status_code == 200

	# Not enough rights to register user
	auth.login('test_zone')
	response = auth.post('/user/register', data={'username': 'somenewuser', 'password': 'somenewuser'})
	assert response.status_code == 403

	# Not enough rights to register user
	auth.login('registered')
	response = auth.post('/user/register', data={'username': 'somenewuser', 'password': 'somenewuser'})
	assert response.status_code == 403

def test_info(auth):
	# Test info about user
	auth.login('test_admin')
	response = auth.get('/user/info')
	assert response.status_code == 200

	response = auth.get('/user/info/test_admin')
	assert response.status_code == 200

	auth.login('test_zone')
	response = auth.get('/user/info')
	assert response.status_code == 200

	response = auth.get('/user/info/test_admin')
	assert response.status_code == 403

	response = auth.get('/user/info/nonexistent')
	assert response.status_code == 403