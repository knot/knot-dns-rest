# from random import choice, randrange
# from string import ascii_letters, digits
# from urllib.parse import quote

from flask.json import dumps

def test_zone_status(auth):
	auth.login('test_admin')
	zonename = 'test.'

	# Get status of every zone
	response = auth.get('/control/zone/status')
	assert response.status_code == 200

	# Get status of specific zone
	response = auth.get(f'/control/zone/status/{zonename}')
	assert response.status_code == 200

	# Get status of nonexistent zone
	response = auth.get(f'/control/zone/status/nonexistent')
	assert response.status_code == 404

	# Get status of specific zone with filters
	response = auth.get(f'/control/zone/status/{zonename}?filters=cefrst')
	assert response.status_code == 200

	# Get status of specific zone with unsupported filters
	response = auth.get(f'/control/zone/status/{zonename}?filters=a')
	assert response.status_code == 400

	# Get status of specific zone
	json_struct = {
		"zones": ["test"]
	}
	response = auth.post('/control/zone/status', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200

	# Get status of nonexistent zone
	json_struct = {
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/status', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 404

	# Get status of multiple zones
	json_struct = {
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/status', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200

def test_zone_reload(auth):
	auth.login('test_admin')
	zonename = 'test.'

	# Reload of every zone
	response = auth.get('/control/zone/reload')
	assert response.status_code == 200

	# Reload of specific zone
	response = auth.get(f'/control/zone/reload/{zonename}')
	assert response.status_code == 200

	# Reload of nonexistent zone
	response = auth.get(f'/control/zone/reload/nonexistent')
	assert response.status_code == 404

	# Reload of specific zone
	json_struct = {
		"zones": ["test"]
	}
	response = auth.post('/control/zone/reload', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200

	# Reload of nonexistent zone
	json_struct = {
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/reload', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 404

	# Reload of multiple zones
	json_struct = {
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/reload', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200

def test_zone_refresh(auth):
	auth.login('test_admin')
	zonename = 'test.'

	# Refresh of every zone
	response = auth.get('/control/zone/refresh')
	assert response.status_code == 200

	# Refresh of specific zone (primary zone)
	response = auth.get(f'/control/zone/refresh/{zonename}')
	assert response.status_code == 400

	# Refresh of nonexistent zone
	response = auth.get(f'/control/zone/refresh/nonexistent')
	assert response.status_code == 404

	# Refresh of specific zone (primary zone)
	json_struct = {
		"zones": ["test"]
	}
	response = auth.post('/control/zone/refresh', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 400

	# Refresh of nonexistent zone
	json_struct = {
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/refresh', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 404

	# Refresh of multiple zones (primary zones)
	json_struct = {
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/refresh', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 400

def test_zone_retransfer(auth):
	auth.login('test_admin')
	zonename = 'test.'

	# Retransfer of every zone
	response = auth.get('/control/zone/retransfer')
	assert response.status_code == 200

	# Retransfer of specific zone (primary zone)
	response = auth.get(f'/control/zone/retransfer/{zonename}')
	assert response.status_code == 400

	# Retransfer of nonexistent zone
	response = auth.get(f'/control/zone/retransfer/nonexistent')
	assert response.status_code == 404

	# Retransfer of specific zone (primary zone)
	json_struct = {
		"zones": ["test"]
	}
	response = auth.post('/control/zone/retransfer', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 400

	# Retransfer of nonexistent zone
	json_struct = {
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/retransfer', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 404

	# Retransfer of multiple zones (primary zones)
	json_struct = {
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/retransfer', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 400

def test_zone_notify(auth):
	auth.login('test_admin')
	zonename = 'test.'

	# Notify every zone
	response = auth.get('/control/zone/notify')
	assert response.status_code == 200

	# Notify specific zone (primary zone)
	response = auth.get(f'/control/zone/notify/{zonename}')
	assert response.status_code == 200

	# Notify nonexistent zone
	response = auth.get(f'/control/zone/notify/nonexistent')
	assert response.status_code == 404

	# Notify specific zone (primary zone)
	json_struct = {
		"zones": ["test"]
	}
	response = auth.post('/control/zone/notify', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200

	# Notify nonexistent zone
	json_struct = {
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/notify', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 404

	# Notify multiple zones (primary zones)
	json_struct = {
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/notify', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200

def test_zone_flush(auth):
	auth.login('test_admin')
	zonename = 'test.'

	# Flush every zone
	response = auth.get('/control/zone/flush')
	assert response.status_code == 200

	# Flush specific zone (primary zone)
	response = auth.get(f'/control/zone/flush/{zonename}')
	assert response.status_code == 200

	# Flush nonexistent zone
	response = auth.get(f'/control/zone/flush/nonexistent')
	assert response.status_code == 404

	# Flush specific zone (primary zone)
	json_struct = {
		"zones": ["test"]
	}
	response = auth.post('/control/zone/flush', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200

	# Flush nonexistent zone
	json_struct = {
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/flush', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 404

	# Flush multiple zones (primary zones)
	json_struct = {
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/flush', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200

def test_zone_backup(knot, auth):
	backup_no = 0
	auth.login('test_admin')
	zonename = 'test.'

	# Backup of every zone (missing output dir)
	response = auth.get('/control/zone/backup?flags=B')
	assert response.status_code == 400

	# Backup of every zone
	response = auth.get(f'/control/zone/backup?dir={knot.storage}/backup-{backup_no:>03}&flags=B')
	assert response.status_code == 200
	backup_no += 1

	# Backup of specific zone
	response = auth.get(f'/control/zone/backup/{zonename}?dir={knot.storage}/backup-{backup_no:>03}&flags=B')
	assert response.status_code == 200
	backup_no += 1

	# Backup nonexistent zone
	response = auth.get(f'/control/zone/backup/nonexistent&flags=B')
	assert response.status_code == 400
	
	# Backup nonexistent zone (with output dir)
	response = auth.get(f'/control/zone/backup/nonexistent?dir={knot.storage}/backup-{backup_no:>03}&flags=B')
	assert response.status_code == 404
	backup_no += 1

	# Backup specific zone
	json_struct = {
		"flags": 'B',
		"dir":   f"{knot.storage}/backup-{backup_no:>03}",
		"zones": ["test"]
	}
	response = auth.post('/control/zone/backup', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200
	backup_no += 1

	# Backup nonexistent zone
	json_struct = {
		"flags": 'B',
		"dir":   f"{knot.storage}/backup-{backup_no:>03}",
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/backup', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 404
	backup_no += 1

	# Backup multiple zones (primary zones)
	json_struct = {
		"flags": 'B',
		"dir":   f"{knot.storage}/backup-{backup_no:>03}",
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/backup', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200
	backup_no += 1

def test_zone_restore(knot, auth):
	auth.login('test_admin')
	zonename = 'test.'

	# Restore every zone
	response = auth.get('/control/zone/restore')
	assert response.status_code == 400

	# Restore specific zone
	response = auth.get(f'/control/zone/restore/{zonename}')
	assert response.status_code == 400

	# Restore nonexistent zone
	response = auth.get(f'/control/zone/restore/nonexistent')
	assert response.status_code == 400

	# Restore from backup of every zone
	response = auth.get(f'/control/zone/backup?flags=B&dir={knot.storage}/backup-000')
	assert response.status_code == 200
	response = auth.get(f'/control/zone/restore?dir={knot.storage}/backup-000&flags=B')
	assert response.status_code == 200

	# Restore specific zone
	json_struct = {
		"zones": ["test"]
	}
	response = auth.post('/control/zone/restore', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 400

	# Restore specific zone from all zone backup
	json_struct = {
		"dir": f'{knot.storage}/backup-000',
		"flags": 'B',
		"zones": ["test"]
	}
	response = auth.post('/control/zone/restore', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200

	# Restore nonexistent zone
	json_struct = {
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/restore', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 400

	# Restore nonexistent zone
	json_struct = {
		"dir": f'{knot.storage}/backup-000',
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/restore', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 404

	# Restore multiple zones
	json_struct = {
		"dir": f'{knot.storage}/backup-000',
		"flags": 'B',
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/restore', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200

	# Restore from not existent backup
	response = auth.get(f'/control/zone/backup/{zonename}?flags=B&dir={knot.storage}/backup-001')
	assert response.status_code == 200
	json_struct = {
		"dir": f'{knot.storage}/backup-001',
		"flags": 'B',
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/restore', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 404

	# Restore multiple zones from backup
	json_struct = {
		"dir": f'{knot.storage}/backup-000',
		"flags": 'B',
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/restore', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200

def test_zone_sign(auth):
	auth.login('test_admin')
	zonename = 'test.'

	# Sign every zone
	response = auth.get('/control/zone/sign')
	assert response.status_code == 200

	# Sign specific zone (primary zone), no DNSSEC
	response = auth.get(f'/control/zone/sign/{zonename}')
	assert response.status_code == 400

	# Sign nonexistent zone
	response = auth.get(f'/control/zone/sign/nonexistent')
	assert response.status_code == 404

	# Sign specific zone (primary zone), no DNSSEC
	json_struct = {
		"zones": ["test"]
	}
	response = auth.post('/control/zone/sign', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 400

	# Sign nonexistent zone
	json_struct = {
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/sign', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 404

	# Sign multiple zones (primary zones), no DNSSEC
	json_struct = {
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/sign', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 400

def test_zone_validate(auth):
	auth.login('test_admin')
	zonename = 'test.'

	# Validate every zone
	response = auth.get('/control/zone/validate')
	assert response.status_code == 200

	# Validate specific zone (primary zone), no DNSSEC
	response = auth.get(f'/control/zone/validate/{zonename}')
	assert response.status_code == 200

	# Validate nonexistent zone
	response = auth.get(f'/control/zone/validate/nonexistent')
	assert response.status_code == 404

	# Validate specific zone (primary zone), no DNSSEC
	json_struct = {
		"zones": ["test"]
	}
	response = auth.post('/control/zone/validate', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200

	# Validate nonexistent zone
	json_struct = {
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/validate', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 404

	# Validate multiple zones (primary zones), no DNSSEC
	json_struct = {
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/validate', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200

def test_zone_keys_load(auth):
	auth.login('test_admin')
	zonename = 'test.'

	# Keys-load every zone
	response = auth.get('/control/zone/keys-load')
	assert response.status_code == 200

	# Keys-load specific zone (primary zone), no DNSSEC
	response = auth.get(f'/control/zone/keys-load/{zonename}')
	assert response.status_code == 400

	# Keys-load nonexistent zone
	response = auth.get(f'/control/zone/keys-load/nonexistent')
	assert response.status_code == 404

	# Keys-load specific zone (primary zone), no DNSSEC
	json_struct = {
		"zones": ["test"]
	}
	response = auth.post('/control/zone/keys-load', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 400

	# Keys-load nonexistent zone
	json_struct = {
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/keys-load', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 404

	# Keys-load multiple zones (primary zones), no DNSSEC
	json_struct = {
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/keys-load', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 400

def test_zone_key_rollover(auth):
	auth.login('test_admin')
	zonename = 'test.'

	# Key-rollover every zone (missing arguments)
	response = auth.get('/control/zone/key-rollover')
	assert response.status_code == 404

	# Key-rollover specific zone (missing arguments)
	response = auth.get(f'/control/zone/key-rollover/{zonename}')
	assert response.status_code == 404

	# Key-rollover nonexistent zone
	response = auth.get(f'/control/zone/key-rollover/nonexistent')
	assert response.status_code == 404

	# Key-rollover specific zone (not supported)
	response = auth.get(f'/control/zone/key-rollover/{zonename}/ksk')
	assert response.status_code == 400

def test_zone_ksk_submitted(auth):
	auth.login('test_admin')
	zonename = 'test.'

	# KSK-submitted every zone (not supported)
	response = auth.get('/control/zone/ksk-submitted')
	assert response.status_code == 400

	# KSK-submitted specific zone (primary zone), no DNSSEC
	response = auth.get(f'/control/zone/ksk-submitted/{zonename}')
	assert response.status_code == 400

	# KSK-submitted nonexistent zone
	response = auth.get(f'/control/zone/ksk-submitted/nonexistent')
	assert response.status_code == 400

	# KSK-submitted specific zone (primary zone), no DNSSEC
	json_struct = {
		"zones": ["test"]
	}
	response = auth.post('/control/zone/ksk-submitted', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 400

	# KSK-submitted nonexistent zone
	json_struct = {
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/ksk-submitted', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 400

	# KSK-submitted multiple zones (primary zones), no DNSSEC
	json_struct = {
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/ksk-submitted', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 400

def test_zone_freeze_thaw(auth):
	auth.login('test_admin')
	zonename = 'test.'

	# Freeze every zone (not supported)
	response = auth.get('/control/zone/freeze')
	assert response.status_code == 200
	response = auth.get('/control/zone/thaw')
	assert response.status_code == 200

	# Freeze specific zone (primary zone), no DNSSEC
	response = auth.get(f'/control/zone/freeze/{zonename}')
	assert response.status_code == 200
	response = auth.get(f'/control/zone/thaw/{zonename}')
	assert response.status_code == 200

	# Freeze nonexistent zone
	response = auth.get(f'/control/zone/freeze/nonexistent')
	assert response.status_code == 404

	# Freeze specific zone (primary zone), no DNSSEC
	json_struct = {
		"zones": ["test"]
	}
	response = auth.post('/control/zone/freeze', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200
	response = auth.get(f'/control/zone/thaw/{zonename}')
	assert response.status_code == 200

	# Freeze nonexistent zone
	json_struct = {
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/freeze', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 404

	# Freeze multiple zones (primary zones), no DNSSEC
	json_struct = {
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/freeze', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200
	response = auth.post('/control/zone/thaw', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200

def test_zone_xfr_freeze_thaw(auth):
	auth.login('test_admin')
	zonename = 'test.'

	# Freeze every zone (not supported)
	response = auth.get('/control/zone/xfr-freeze')
	assert response.status_code == 200
	response = auth.get('/control/zone/xfr-thaw')
	assert response.status_code == 200

	# Freeze specific zone (primary zone), no DNSSEC
	response = auth.get(f'/control/zone/xfr-freeze/{zonename}')
	assert response.status_code == 200
	response = auth.get(f'/control/zone/xfr-thaw/{zonename}')
	assert response.status_code == 200

	# Freeze nonexistent zone
	response = auth.get(f'/control/zone/xfr-freeze/nonexistent')
	assert response.status_code == 404

	# Freeze specific zone (primary zone), no DNSSEC
	json_struct = {
		"zones": ["test"]
	}
	response = auth.post('/control/zone/xfr-freeze', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200
	response = auth.get(f'/control/zone/xfr-thaw/{zonename}')
	assert response.status_code == 200

	# Freeze nonexistent zone
	json_struct = {
		"zones": ["noexistent"]
	}
	response = auth.post('/control/zone/xfr-freeze', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 404

	# Freeze multiple zones (primary zones), no DNSSEC
	json_struct = {
		"zones": ["test", "test2"]
	}
	response = auth.post('/control/zone/xfr-freeze', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200
	response = auth.post('/control/zone/xfr-thaw', data=dumps(json_struct), content_type='application/json')
	assert response.status_code == 200