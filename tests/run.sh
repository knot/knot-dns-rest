#!/usr/bin/env sh

usage() {
	echo "Run functional and coverage tests are generate output"
	echo ""
	echo "Before run of script you need to set 'KNOTD_EXEC' and" \
	     "'KNOT_LIB' environment variables setup with paths to 'knotd'" \
	     "binary and 'libknot.so' shared object"
	echo ""
	echo "Usage:"
	echo "  Run tests and generate output"
	echo "    ./run.sh [-H]"
	echo "  Show help"
	echo "    ./run.sh -h"
	echo ""
	echo "Parameters:"
	echo "  -h  -  Show help"
	echo "  -H  -  Generate HTML ouutput instead of text based"
}

html=1
while getopts ":hH" o; do
	case $o in
		h)
			usage
			exit 0
			;;
		H)
			html=0
			;;
		*)
			usage
			exit 1
	esac
done

if test -z ${KNOTD_EXEC}; then
	echo "ERROR: Missing environment variable 'KNOTD_EXEC' which defines" \
	     "the path of the 'knotd' executable" >&2
	exit 1
fi
if test -z ${KNOT_LIB}; then
	echo "ERROR: Missing environment variable 'KNOT_LIB' which defines" \
	     "the path of the 'libknot.so' object file" >&2
	exit 1
fi

pytest
coverage run -m pytest
if test $html -eq 1; then
	coverage report
else
	coverage html
fi