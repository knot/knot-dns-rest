def test_login(auth):
	assert not auth.token

	auth.login()
	assert auth.token

def test_authorization(auth):
	auth.login()
	response = auth.get('/zones')
	assert response.default_status == 200