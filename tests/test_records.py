from random import choice, randrange
from string import ascii_letters, digits
from urllib.parse import quote

from flask.json import dumps

def _random_owner():
	return ''.join(choice(ascii_letters + digits) for _ in range(20))

def test_get_records(auth):
	auth.login('test_admin')
	zonename = 'test.'

	# Get from nonexistent zone
	response = auth.get('/zones/nonexistent/records')
	assert response.status_code == 404

	# Get all records of the zone
	response = auth.get(f'/zones/{zonename}/records')
	assert response.status_code == 200

	record = choice(response.json)
	# Get records of specific parameters
	response = auth.get(f'/zones/{zonename}/records/{quote(record["name"])}')
	assert response.status_code == 200
	response = auth.get(f'/zones/{zonename}/records/{quote(record["name"])}/{record["rtype"]}')
	assert response.status_code == 200
	response = auth.get(f'/zones/{zonename}/records/{quote(record["name"])}/{record["rtype"]}/{quote(record["data"])}')
	assert response.status_code == 200
	response = auth.get(f'/zones/{zonename}/records/{quote(record["name"])}/{record["rtype"]}/{quote(record["data"])}/{record["ttl"]}')
	assert response.status_code == 200

	# Get records with parameters in URL
	response = auth.get(f'/zones/{zonename}/records?name={quote(record["name"])}&rtype={record["rtype"]}&data={quote(record["data"])}&ttl={record["ttl"]}')
	assert response.status_code == 200

	# Get records with parameters in HTTP header
	response = auth.get(f'/zones/{zonename}/records', data={"name": record["name"], "rtype": record["rtype"], "data": record["data"], "ttl": record["ttl"]})
	assert response.status_code == 200

	# Get records with filtered parameters
	response = auth.get(f'/zones/{zonename}/records?data={quote(record["data"])}&ttl={record["ttl"]}')
	assert response.status_code == 200

	# Get nonexistent record
	data = _random_owner()
	response = auth.get(f'/zones/{zonename}/records', data={"name": record["name"], "data": data, "ttl": randrange(65536)})
	assert response.status_code == 404

	# Get nonexistent zone record
	owner = _random_owner()
	response = auth.get(f'/zones/{zonename}/records', data={"name": owner, "data": data, "ttl": randrange(65536)})
	assert response.status_code == 404

	zonename = 'test'
	# Get records of shortformed zonename
	response = auth.get(f'/zones/{zonename}/records/{quote(record["name"])}/{record["rtype"]}/{quote(record["data"])}/{record["ttl"]}')
	assert response.status_code == 200

def _random_ip4():
	return '.'.join(str(randrange(256)) for _ in range(4))

def test_put_records(auth):
	auth.login('test_admin')
	zonename = 'test.'

	owner = _random_owner()
	# Put record with insufficient parameters
	response = auth.put(f'/zones/{zonename}/records/{quote(owner)}')
	assert response.status_code == 400

	ip = _random_ip4()
	ttl = randrange(65536)
	# Put record to nonexistent zone
	response = auth.put(f'/zones/nonexistent/records/{quote(owner)}/A/{quote(ip)}/{ttl}')
	assert response.status_code == 404

	# Put record with specified TTL
	response = auth.put(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}/{ttl}')
	assert response.status_code == 201

	# Put the same record
	response = auth.put(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}/{ttl}')
	assert response.status_code == 409

	ip = _random_ip4()
	# Put record with same owner but without TTL
	response = auth.put(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}')
	assert response.status_code == 201

	ip = _random_ip4()
	ttl = randrange(65536)
	# Get records with parameters in URL
	response = auth.put(f'/zones/{zonename}/records?name={quote(owner)}&rtype=A&data={quote(ip)}&ttl={ttl}')
	assert response.status_code == 201
	response = auth.get(f'/zones/{zonename}/records?name={quote(owner)}&rtype=A')
	assert len(response.json) == 3
	# Test change of TTL for whole RRset
	for record in response.json:
		assert int(record['ttl']) == ttl

	ip = _random_ip4()
	# Get records with parameters in HTTP header
	response = auth.put(f'/zones/{zonename}/records', data={"name": owner, "rtype": "A", "data": ip, "ttl": ttl})
	assert response.status_code == 201

def test_delete_records(auth):
	auth.login('test_admin')
	zonename = 'test.'
	
	owner = _random_owner()
	ip = _random_ip4()
	# Delete from nonexistent zone
	response = auth.delete(f'/zones/nonexistent/records/{owner}')
	assert response.status_code == 404

	response = auth.put(f'/zones/{zonename}/records/{owner}/A/{ip}/{randrange(65536)}')
	assert response.status_code == 201
	# Delete record with specified parameters
	response = auth.delete(f'/zones/{zonename}/records/{owner}')
	assert response.status_code == 200

	owner = _random_owner()
	ip = _random_ip4()
	auth.put(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}/{randrange(65536)}')
	# Delete record with parameters specified in URL
	response = auth.delete(f'/zones/{zonename}/records?name={quote(owner)}&rtype=A&data={quote(ip)}')
	assert response.status_code == 200

	owner = _random_owner()
	ip = _random_ip4()
	auth.put(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}/{randrange(65536)}')
	# Delete record with parameters specified in HTTP header
	response = auth.delete(f'/zones/{zonename}/records', data={"name": owner, "rtype": "A", "data": ip})
	assert response.status_code == 200

def test_patch_records(auth):
	auth.login('test_admin')
	zonename = 'test.'
	
	owner = _random_owner()
	ip = _random_ip4()
	new_owner = _random_owner()
	new_ip = _random_ip4()
	# Edit record in nonexistent zone
	response = auth.patch(f'/zones/nonexistent/records/{quote(owner)}/A/{quote(ip)}?name={quote(new_owner)}&data={quote(new_ip)}')
	assert response.status_code == 404

	auth.put(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}/{randrange(65536)}')
	# Edit record with parameters specified in URL
	response = auth.patch(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}?name={quote(new_owner)}&data={quote(new_ip)}')
	assert response.status_code == 200
	response = auth.get(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}')
	assert response.status_code == 404
	owner = new_owner
	ip = new_ip
	response = auth.get(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}')
	assert response.status_code == 200

	owner = _random_owner()
	ip = _random_ip4()
	auth.put(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}/{randrange(65536)}')
	# Edit record with parameters specified in HTTP header
	new_owner = _random_owner()
	new_ip = _random_ip4()
	response = auth.patch(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}', data={"name":quote(new_owner), "data": quote(new_ip)})
	assert response.status_code == 200
	response = auth.get(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}')
	assert response.status_code == 404
	owner = new_owner
	ip = new_ip
	response = auth.get(f'/zones/{zonename}/records/{quote(owner)}/A/{quote(ip)}')
	assert response.status_code == 200


	response = auth.get(f'/zones/{zonename}/records', data={"rtype":"SOA"})
	assert response.status_code == 200
	assert len(response.json) == 1
	json = response.json[0]
	assert json['rtype'] == 'SOA'
	new_data = json['data'].split()
	assert len(new_data) == 7
	# Edit SOA record
	new_data[2] = str(int(new_data[2]) + 1) # Increment serial
	response = auth.patch(f'/zones/{zonename}/records/{quote(json["name"])}/SOA/{quote(json["data"])}', data={"data": quote(' '.join(new_data))})
	assert response.status_code == 200

def test_post_records_get(auth):
	auth.login('test_admin')
	zonename = 'test.'

	json_struct = {"method": "GET"}
	# Get from nonexistent zone
	response = auth.post('/zones/nonexistent/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 404

	# Get all records of the zone
	response = auth.post(f'/zones/{zonename}/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 200

	record = choice(response.json)
	# Get records of specific parameters
	json_struct['filter'] = {"name": record["name"]}
	response = auth.post(f'/zones/{zonename}/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 200
	json_struct['filter']['rtype'] = record["rtype"]
	response = auth.post(f'/zones/{zonename}/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 200
	json_struct['filter']['data'] = record["data"]
	response = auth.post(f'/zones/{zonename}/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 200
	json_struct['filter']['ttl'] = record["ttl"]
	response = auth.post(f'/zones/{zonename}/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 200

	# Get records of shortformed zonename
	response = auth.post(f'/zones/test/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 200

	# Get from nonexistent zone
	response = auth.post('/zones/nonexistent/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 404

	# Get nonexistent record
	json_struct['filter']['data'] = _random_owner()
	json_struct['filter']['ttl'] = randrange(65536)
	response = auth.post(f'/zones/{zonename}/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 404

	# Get nonexistent zone record
	json_struct['filter']['name'] = _random_owner()
	response = auth.post(f'/zones/{zonename}/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 404

def test_post_records_put(auth):
	auth.login('test_admin')
	zonename = 'test.'

	owner = _random_owner()
	json_struct = {
			"method": "PUT",
			"input": {
				"name": owner
			}
		}
	# Put record with insufficient parameters
	response = auth.post(f'/zones/{zonename}/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 400

	json_struct['input']['rtype'] = 'A'
	json_struct['input']['data'] = _random_ip4()
	json_struct['input']['ttl'] = randrange(65536)

	# Put into nonexistent zone
	response = auth.post(f'/zones/nonexistent/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 404
	# Put record with specified TTL
	response = auth.post(f'/zones/{zonename}/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 200
	
	json_struct['input']['data'] = _random_ip4()
	del json_struct['input']['ttl']
	# Put record with same owner but without TTL
	response = auth.post(f'/zones/{zonename}/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 200

def test_post_records_delete(auth):
	auth.login('test_admin')
	zonename = 'test.'
	
	owner = _random_owner()
	ip = _random_ip4()
	response = auth.put(f'/zones/{zonename}/records/{owner}/A/{ip}/{randrange(65536)}')
	assert response.status_code == 201

	json_struct = {
		"method": "DELETE",
		"filter": {
			"name": owner
		}
	}
	# Delete record from nonexistent zone
	response = auth.post(f'/zones/nonexistent/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 404

	# Delete record with specified parameters
	response = auth.post(f'/zones/{zonename}/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 200

	# Delete where filter don't match any record
	response = auth.post(f'/zones/{zonename}/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 404

def test_post_records_patch(auth):
	auth.login('test_admin')
	zonename = 'test.'
	
	json_struct = {
		"method": "PATCH",
		"filter": {
			"name": _random_owner(),
			"rtype": 'A',
			"ttl": randrange(65536),
			"data": _random_ip4()
			
		},
		"input": {
			"name": _random_owner(),
			"data": _random_ip4()
		}
	}
	response = auth.put(f"/zones/{zonename}/records/{quote(json_struct['filter']['name'])}/{json_struct['filter']['rtype']}/{quote(json_struct['filter']['data'])}/{json_struct['filter']['ttl']}")
	assert response.status_code == 201

	# Edit record from nonexistent zone
	response = auth.post(f'/zones/nonexistent/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 404
	# Edit record
	response = auth.post(f'/zones/{zonename}/records', data=dumps([json_struct]), content_type='application/json')
	assert response.status_code == 200
	response = auth.get(f"/zones/{zonename}/records/{quote(json_struct['filter']['name'])}/{json_struct['filter']['rtype']}/{quote(json_struct['filter']['data'])}/{json_struct['filter']['ttl']}")
	assert response.status_code == 404
	response = auth.get(f"/zones/{zonename}/records/{quote(json_struct['input']['name'])}/{json_struct['filter']['rtype']}/{quote(json_struct['input']['data'])}/{json_struct['filter']['ttl']}")
	assert response.status_code == 200