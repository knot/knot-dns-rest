from random import choice
from string import ascii_letters

def test_get_zones(auth):
	## Test API
	auth.login('test_admin')

	# try to get all zones
	response = auth.get('/zones')
	assert response.status_code == 200
	
	zonename = 'test.'
	# Get zone of specific name in path
	response = auth.get(f'/zones/{zonename}')
	assert response.status_code == 200
	# Get zone of specific name in URL parameters
	response = auth.get(f'/zones?zone={zonename}')
	assert response.status_code == 200
	# Get zone of specific name in header
	response = auth.get('/zones', data={"zone": zonename})
	assert response.status_code == 200

	zonename = ''.join(choice(ascii_letters) for _ in range(20))
	# Get nonexistent zone
	response = auth.get(f'/zones/{zonename}')
	assert response.status_code == 404

	zonename = 'test'
	# Get zone using short-form
	response = auth.get(f'/zones/{zonename}')
	assert response.status_code == 200

	## Test access rights
	auth.login()

	response = auth.get('/zones/test')
	assert response.status_code == 403

	auth.login('test_zone')

	response = auth.get('/zones/test')
	assert response.status_code == 200