from secrets import token_hex

from pytest import raises

from knot_rest.auth import register_user
from knot_rest.exceptions import KnotRestUserError

def test_database(app):
	# Test unique 'username' column
	with raises(KnotRestUserError):
		register_user("test", "password")

	# Test store user after previous error
	register_user(token_hex(5), "password")
	assert True