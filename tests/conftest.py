from base64 import b64encode

import pytest

from knot_rest import create_app, db
from knot_rest.auth import add_admin, add_zoneacl, register_user

from tests.knot import Knot

class AuthActions(object):
	def __init__(self, client):
		self._client = client
		self.token = None

	def login(self, username='test', password=None):
		if not password:
			password = username
		credentials = b64encode(bytearray(f"{username}:{password}", 'utf-8')).decode('utf-8')
		response = self._client.get(
			'/user/login',
			headers={"Authorization": f"Basic {credentials}"}
		)
		self.token = response.json['token']
		return True
	
	def get(self, *args, **kwargs):
		return self._client.get(headers={"Authorization": f"Bearer {self.token}"}, *args, **kwargs)

	def post(self, *args, **kwargs):
		return self._client.post(headers={"Authorization": f"Bearer {self.token}"}, *args, **kwargs)

	def put(self, *args, **kwargs):
		return self._client.put(headers={"Authorization": f"Bearer {self.token}"}, *args, **kwargs)

	def patch(self, *args, **kwargs):
		return self._client.patch(headers={"Authorization": f"Bearer {self.token}"}, *args, **kwargs)

	def delete(self, *args, **kwargs):
		return self._client.delete(headers={"Authorization": f"Bearer {self.token}"}, *args, **kwargs)


@pytest.fixture
def knot():
	server = Knot()
	server.start()
	yield server
	server.stop()

@pytest.fixture
def app(knot : Knot):
	test_config = {
		'database': 'sqlite:///' + knot.storage + '/database.db',
		'socket': {
			'path': knot.socket_path
		}
	}
	app = create_app(test_config=test_config)

	db.create_all()
	register_user('test_admin', 'test_admin')
	add_admin('test_admin')
	register_user('test', 'test')
	register_user('test_zone', 'test_zone')
	add_zoneacl('test_zone', 'test.')

	yield app

	db.session.remove()

@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()

@pytest.fixture
def auth(client):
	return AuthActions(client)